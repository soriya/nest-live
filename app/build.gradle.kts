plugins {
    id("com.android.application")
    id("org.jetbrains.kotlin.android")
    id("kotlin-kapt")
    id("dagger.hilt.android.plugin")
//    id("com.google.dagger.hilt.android")
}

android {
    namespace = "com.soriya.nestlive"
    compileSdk = 34

    defaultConfig {
        applicationId = "com.soriya.nestlive"
        minSdk = 24
        targetSdk = 28
        versionCode = 1
        versionName = "1.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        vectorDrawables {
            useSupportLibrary = true
        }
        externalNativeBuild {
            cmake {
                cppFlags += ""
            }
        }
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }
    kotlinOptions {
        jvmTarget = "17"
    }
    buildFeatures {
        compose = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = "1.4.3"
    }
    packaging {
        resources {
            excludes += "/META-INF/{AL2.0,LGPL2.1}"
        }
    }
    externalNativeBuild {
        cmake {
            path = file("src/main/cpp/CMakeLists.txt")
            version = "3.22.1"
        }
    }

//    sourceSets.all {
//        jniLibs.srcDir("libs")
//    }
}

dependencies {
//    implementation(fileTree(mapOf("include" to listOf("*.jar"), "dir" to "libs")))

//    implementation(project(":ijkplayer"))
    implementation(project(":gsyplayer"))

    implementation("androidx.core:core-ktx:1.9.0")
    implementation("androidx.lifecycle:lifecycle-runtime-ktx:2.6.1")
    implementation("androidx.activity:activity-compose:1.7.0")
    implementation(platform("androidx.compose:compose-bom:2023.03.00"))
    implementation("androidx.compose.ui:ui")
    implementation("androidx.compose.ui:ui-graphics")
    implementation("androidx.compose.ui:ui-tooling-preview")
    implementation("androidx.compose.material:material")
//    implementation("androidx.compose.material3:material3")
    implementation("androidx.compose.material:material-icons-extended:1.2.1")
    testImplementation("junit:junit:4.13.2")
    androidTestImplementation("androidx.test.ext:junit:1.1.5")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.5.1")
    androidTestImplementation(platform("androidx.compose:compose-bom:2023.03.00"))
    androidTestImplementation("androidx.compose.ui:ui-test-junit4")
    debugImplementation("androidx.compose.ui:ui-tooling")
    debugImplementation("androidx.compose.ui:ui-test-manifest")

    // navigation
    val nav_version = "2.6.0"
    implementation("androidx.navigation:navigation-compose:$nav_version")

    // Coil
    implementation("io.coil-kt:coil-compose:2.2.2")

    // 金山直播SDK
    // https://mvnrepository.com/artifact/com.ksyun.media/libksylive-java
    implementation("com.ksyun.media:libksylive-java:3.0.4")
    // https://mvnrepository.com/artifact/com.ksyun.media/libksylive-arm64
    implementation("com.ksyun.media:libksylive-arm64:3.0.4")

    // 约束布局
    implementation("androidx.constraintlayout:constraintlayout-compose:1.0.1")

    implementation("com.google.accompanist:accompanist-systemuicontroller:0.28.0")

//    implementation("androidx.compose.material3:material3-window-size-class:1.0.0")

    // Retrofit
    implementation("com.squareup.retrofit2:retrofit:2.9.0")
    implementation("com.squareup.retrofit2:converter-gson:2.9.0")

    // Room
    val room_version = "2.5.1"
    implementation("androidx.room:room-runtime:$room_version")
    implementation("androidx.room:room-ktx:$room_version")
    kapt("androidx.room:room-compiler:$room_version")

    // Hilt
    implementation("com.google.dagger:hilt-android:2.43.2")
//    annotationProcessor("com.google.dagger:hilt-android-compiler:2.43.2")
    kapt("com.google.dagger:hilt-compiler:2.43.2")
//    implementation("androidx.hilt:hilt-lifecycle-viewmodel:1.0.0-alpha03")
    implementation("androidx.hilt:hilt-navigation-compose:1.0.0")

    // Paging
    implementation("androidx.paging:paging-compose:3.2.1")

    // Matisse // Compose无法拿到图片结果
//    implementation("com.zhihu.android:matisse:0.5.3-beta3")

    implementation("io.netty:netty-all:4.1.39.Final")
    implementation(kotlin("reflect"))

//    implementation("com.vanniktech:android-image-cropper:4.5.0")
    implementation("com.github.yalantis:ucrop:2.2.8")

}