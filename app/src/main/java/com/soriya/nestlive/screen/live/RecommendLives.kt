package com.soriya.nestlive.screen.live

import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.viewmodel.compose.viewModel
import com.soriya.nestlive.component.LiveCard
import com.soriya.nestlive.component.ScreenColumnContent
import com.soriya.nestlive.constant.UIValue
import com.soriya.nestlive.viewmodel.LiveModel

@Composable
fun RecommendLivesScreen(
    toLiveDetail: (Long) -> Unit
) {
    val model: LiveModel = hiltViewModel()
    val state by model.recommendLiveList.collectAsState()

    ScreenColumnContent {
        // TODO Paging
        LazyColumn(
            modifier = Modifier.padding(vertical = UIValue.VERTICAL_PADDING)
        ) {
            items(state) {
                LiveCard(data = it, onClick = toLiveDetail)
            }
        }
    }
}