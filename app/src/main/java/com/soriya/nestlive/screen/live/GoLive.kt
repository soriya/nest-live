package com.soriya.nestlive.screen.live

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Icon
import androidx.compose.material.LocalContentColor
import androidx.compose.material.MaterialTheme
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowForwardIos
import androidx.compose.material.icons.filled.PhotoCamera
import androidx.compose.material.icons.filled.VideoLibrary
import androidx.compose.material.icons.filled.VideogameAsset
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.soriya.nestlive.component.MediumTitle
import com.soriya.nestlive.component.ScreenColumnContent
import com.soriya.nestlive.component.SmallText
import com.soriya.nestlive.component.SmallerAdditionalText
import com.soriya.nestlive.component.SmallerText
import com.soriya.nestlive.constant.UIValue
import com.soriya.nestlive.router.ScreenPath
import com.soriya.nestlive.R

@Composable
fun GoLiveScreen(
    toStreamInfo: (ScreenPath) -> Unit
) {
    ScreenColumnContent(
        modifier = Modifier
            .padding(vertical = UIValue.VERTICAL_PADDING.times(4))
    ) {
        Box(
            contentAlignment = Alignment.Center,
            modifier = Modifier.fillMaxWidth().weight(1F)
        ) {
            Image(
                painter = painterResource(id = R.mipmap.stream_back),
                contentDescription = "Background",
                modifier = Modifier.size(300.dp)
            )
        }

        LiveButton(
            title = "游戏直播",
            desc = "从移动设备播放流媒体",
            icon = Icons.Default.VideogameAsset,
            onClick = {
                toStreamInfo(ScreenPath.GameStreamInfo)
            }
        )

        Spacer(modifier = Modifier.height(20.dp))

        LiveButton(
            title = "摄像头直播",
            desc = "无论你身在何处，广播你的生活",
            icon = Icons.Default.PhotoCamera,
            onClick = {
                toStreamInfo(ScreenPath.CameraStreamInfo)
            }
        )

        Spacer(modifier = Modifier.height(20.dp))

        LiveButton(
            title = "发布视频",
            desc = "记录日常点点滴滴",
            icon = Icons.Default.VideoLibrary,
            onClick = {
                toStreamInfo(ScreenPath.PublishVideo)
            }
        )

    }
}

@Composable
private fun LiveButton(
    title: String,
    desc: String,
    icon: ImageVector,
    onClick: () -> Unit
) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier
            .clip(RoundedCornerShape(UIValue.LARGE_RADIUS))
            .background(MaterialTheme.colors.surface.copy(0.15F))
            .clickable(onClick = onClick)
            .padding(20.dp)
    ) {
        CompositionLocalProvider(LocalContentColor provides MaterialTheme.colors.primary) {
            Icon(
                imageVector = icon,
                contentDescription = title,
                modifier = Modifier
                    .clip(CircleShape)
                    .background(LocalContentColor.current.copy(alpha = 0.1F))
                    .padding(20.dp)
            )
            Spacer(modifier = Modifier.width(20.dp))
            CompositionLocalProvider(LocalContentColor provides Color.Black) {
                Column(
                    verticalArrangement = Arrangement.SpaceBetween,
                    modifier = Modifier
                        .weight(1F)
                ) {
                    MediumTitle(text = title)
                    SmallerAdditionalText(text = desc)
                }
            }
            Spacer(modifier = Modifier.width(40.dp))
            Icon(imageVector = Icons.Default.ArrowForwardIos, contentDescription = "Go")
        }
    }
}