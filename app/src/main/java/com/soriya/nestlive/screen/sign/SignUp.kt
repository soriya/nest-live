package com.soriya.nestlive.screen.sign

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextFieldDefaults
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AccountCircle
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.soriya.nestlive.component.CustomEditText
import com.soriya.nestlive.component.ScreenColumnContent
import com.soriya.nestlive.constant.UIValue

@Composable
fun SignUpScreen(toSignIn: () -> Unit) {
    ScreenColumnContent(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .fillMaxSize()
    ) {

        SignTop()

        SignUpForm(modifier = Modifier.weight(1f))

        Spacer(modifier = Modifier.height(20.dp))

        SignBottom {
            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.Center
            ) {
                Text(
                    text = "Already have on account? ",
                    textAlign = TextAlign.Center,
                    color = MaterialTheme.colors.surface.copy(0.5F)
                )
                Text(
                    text = "Sign in",
                    textAlign = TextAlign.Center,
                    color = MaterialTheme.colors.primary,
                    fontWeight = FontWeight.Bold,
                    modifier = Modifier.clickable {
                        toSignIn()
                    }
                )
            }
        }
    }
}

@Composable
fun SignUpForm(modifier: Modifier = Modifier) {
    Column(
        modifier = modifier
    ) {
        var accountValue by remember { mutableStateOf("") }

        val editTextBackground: Color = MaterialTheme.colors.surface.copy(alpha = 0.04F)

        CustomEditText(
            value = accountValue,
            onValueChange = {
                accountValue = it
            },
            placeholder = "Account",
            leftIcon = Icons.Default.AccountCircle,
            colors = TextFieldDefaults.textFieldColors(
                backgroundColor = editTextBackground
            ),
            shape = RoundedCornerShape(UIValue.LARGE_RADIUS),
            modifier = Modifier
                .fillMaxWidth()
        )

        Spacer(modifier = Modifier.height(20.dp))

        var passwordValue by remember { mutableStateOf("") }
        CustomEditText(
            value = passwordValue,
            onValueChange = {
                passwordValue = it
            },
            placeholder = "Password",
            leftIcon = Icons.Default.AccountCircle,
            visualTransformation = PasswordVisualTransformation(),
            colors = TextFieldDefaults.textFieldColors(
                backgroundColor = editTextBackground
            ),
            shape = RoundedCornerShape(UIValue.LARGE_RADIUS),
            modifier = Modifier
                .fillMaxWidth()
        )

        Spacer(modifier = Modifier.height(40.dp))

//        Row(
//            modifier = Modifier
//                .fillMaxWidth()
//                .padding(vertical = 16.dp),
//            verticalAlignment = Alignment.CenterVertically,
//            horizontalArrangement = Arrangement.Center
//        ) {
//            Checkbox(
//                checked = false,
//                onCheckedChange = {},
//                colors = CheckboxDefaults.colors(uncheckedColor = MaterialTheme.colors.primary)
//            )
//            Text(text = "Remember Me", color = MaterialTheme.colors.primary, fontSize = 18.sp, fontWeight = FontWeight.Bold)
//        }
        
        Button(
            onClick = { /*TODO*/ },
            modifier = Modifier
                .fillMaxWidth()
                .height(50.dp),
            shape = RoundedCornerShape(50)
        ) {
            Text(text = "Sign up", fontSize = 19.sp)
        }
    }
}