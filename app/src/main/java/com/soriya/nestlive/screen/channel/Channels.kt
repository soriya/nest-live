package com.soriya.nestlive.screen.channel

import android.util.Log
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.paging.compose.collectAsLazyPagingItems
import androidx.paging.compose.itemKey
import com.soriya.nestlive.component.ChannelItem
import com.soriya.nestlive.component.CustomDivider
import com.soriya.nestlive.component.ScreenColumnContent
import com.soriya.nestlive.viewmodel.ChannelModel

@Composable
fun ChannelsScreen(
    toChannelDetail: (Long) -> Unit
) {
    val model: ChannelModel = hiltViewModel()
    val followList by model.channelList.collectAsState()
    val recommendChannel = model.recommendChannelList.collectAsLazyPagingItems()

    ScreenColumnContent {
        LazyColumn {
            items(followList) {
                ChannelItem(data = it, toChannelDetail = toChannelDetail)
            }

            item {
                CustomDivider(text = "以下是推荐的频道")
            }

            items(
                recommendChannel.itemCount,
                key = recommendChannel.itemKey { it.id!! }
            ) {
                val item = recommendChannel[it]
                ChannelItem(data = item!!, isFollow = false, toChannelDetail = toChannelDetail)
            }

            item {
                CustomDivider(text = "已经到底了")
            }
        }
    }
}