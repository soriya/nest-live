package com.soriya.nestlive.screen.live

import android.app.Activity
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.LocalContentColor
import androidx.compose.material.MaterialTheme
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.RemoveRedEye
import androidx.compose.material.icons.filled.Star
import androidx.compose.material.icons.filled.ThumbDown
import androidx.compose.material.icons.filled.ThumbUp
import androidx.compose.material.icons.outlined.Star
import androidx.compose.material.icons.outlined.ThumbUp
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.hilt.navigation.compose.hiltViewModel
import com.shuyu.gsyvideoplayer.builder.GSYVideoOptionBuilder
import com.soriya.nestlive.component.CustomImage
import com.soriya.nestlive.component.CustomOutlinedButton
import com.soriya.nestlive.component.CustomTag
import com.soriya.nestlive.component.GsyPlayer
import com.soriya.nestlive.component.LargeText
import com.soriya.nestlive.component.MediumText
import com.soriya.nestlive.component.MediumTitle
import com.soriya.nestlive.component.ScreenColumnContent
import com.soriya.nestlive.component.SmallText
import com.soriya.nestlive.component.SmallerAdditionalText
import com.soriya.nestlive.constant.UIValue
import com.soriya.nestlive.model.Video
import com.soriya.nestlive.util.WindowUtil
import com.soriya.nestlive.viewmodel.VideoModel

@Composable
fun VideoDetailScreen(
    videoId: Long
) {
    val context = LocalContext.current as Activity

    val videoModel: VideoModel = hiltViewModel()

    val video by videoModel.videoDetail.collectAsState()

    LaunchedEffect(Unit) {
        videoModel.getDetailById(videoId)
    }

    if (video != null) Column {
        val widthDp = WindowUtil.getScreenWidthToDp(context)

        val optionBuilder = GSYVideoOptionBuilder()
            .setCacheWithPlay(true)
            .setVideoTitle(video?.videoName)
            .setIsTouchWiget(true)
            //.setAutoFullWithSize(true)
            .setRotateViewAuto(false)
            .setLockLand(false)
            .setShowFullAnimation(false) // 打开动画
            .setNeedLockFull(true)
            .setSeekRatio(1F)
        GsyPlayer(
            url = video?.path ?: video?.source ?: "",
            optionBuilder = optionBuilder,
            modifier = Modifier
                .fillMaxWidth()
                .height((widthDp * 9 / 16).dp) // 16 : 9
        )

        ScreenColumnContent {
            Author(data = video!!)
            VideoInfo(data = video!!)
        }
    } else {
        Box(
            contentAlignment = Alignment.Center,
            modifier = Modifier
                .fillMaxSize()
                .background(Color.Black.copy(0.3f))
        ) {
            CircularProgressIndicator(
                modifier = Modifier.width(64.dp)
            )
        }
    }
}

@Composable
private fun Author(data: Video) {
    ConstraintLayout(
        modifier = Modifier
            .fillMaxWidth()
            .height(60.dp)
    ) {
        val (avatar, nickname, button) = createRefs()

        CustomImage(
            url = data.avatar,
            contentDescription = "Author Avatar",
            shape = CircleShape,
            modifier = Modifier
                .constrainAs(avatar) {
                    start.linkTo(parent.start)
                    top.linkTo(parent.top)
                    bottom.linkTo(parent.bottom)
                }
                .size(40.dp)
        )

        MediumText(
            text = data.nickname,
            modifier = Modifier
                .constrainAs(nickname) {
                    start.linkTo(avatar.end, margin = UIValue.HORIZONTAL_PADDING)
                    top.linkTo(parent.top)
                    bottom.linkTo(parent.bottom)
                }
        )

        CustomOutlinedButton(
            onClick = { /*TODO*/ },
            modifier = Modifier
                .constrainAs(button) {
                    end.linkTo(parent.end)
                    top.linkTo(parent.top)
                    bottom.linkTo(parent.bottom)
                }
        ) {
            SmallText(text = "关注")
        }
    }
}

@Composable
private fun VideoInfo(data: Video) {
    Column(
        modifier = Modifier.padding(top = UIValue.VERTICAL_PADDING)
    ) {
        LargeText(text = data.videoName)

        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier.padding(vertical = UIValue.VERTICAL_PADDING)
        ) {
            Icon(imageVector = Icons.Default.RemoveRedEye, contentDescription = "Views", tint = MaterialTheme.colors.surface, modifier = Modifier.size(16.dp))
            Spacer(modifier = Modifier.width(4.dp))
            SmallerAdditionalText(text = data.viewers.toString())

            Spacer(modifier = Modifier.width(10.dp))

            SmallerAdditionalText(text = data.createTime)
        }

        data.tags?.let {
            Row(
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier.padding(vertical = UIValue.VERTICAL_PADDING)
            ) {
                it.split("|").forEach {
                    CustomTag(tag = it)
                }
            }
        }


        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier.padding(vertical = UIValue.VERTICAL_PADDING.div(2))
        ) {

            CompositionLocalProvider(LocalContentColor provides Color(0xFF666666)) {
                IconButton(onClick = { /*TODO*/ }) {
                    Icon(
                        imageVector = Icons.Default.ThumbUp,
                        contentDescription = "Like",
                        modifier = Modifier
                            .padding(horizontal = UIValue.HORIZONTAL_PADDING)
                            .size(24.dp)
                    )
                }

                IconButton(onClick = { /*TODO*/ }) {
                    Icon(
                        imageVector = Icons.Default.ThumbDown,
                        contentDescription = "Unlike",
                        modifier = Modifier
                            .padding(horizontal = UIValue.HORIZONTAL_PADDING)
                            .size(24.dp)
                    )
                }

                IconButton(onClick = { /*TODO*/ }) {
                    Icon(
                        imageVector = Icons.Default.Star,
                        contentDescription = "Collect",
                        modifier = Modifier
                            .padding(horizontal = UIValue.HORIZONTAL_PADDING)
                            .size(24.dp)
                    )
                }
            }
        }
    }
}