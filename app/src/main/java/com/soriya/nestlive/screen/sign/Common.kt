package com.soriya.nestlive.screen.sign

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.soriya.nestlive.R
import com.soriya.nestlive.constant.UIValue

@Composable
fun SignTop() {
//    Spacer(modifier = Modifier.height(40.dp))

    Image(
        painter = painterResource(id = R.drawable.logo),
        contentDescription = "Logo",
        modifier = Modifier
            .size(80.dp)
    )

    Text(
        text = "Create New Account",
        fontSize = 24.sp,
        fontWeight = FontWeight.Bold
    )

}

@Composable
fun SignBottom(
    content: @Composable () -> Unit
) {
    Row(
        verticalAlignment = Alignment.CenterVertically
    ) {
        val lineColor = MaterialTheme.colors.surface.copy(0.1F)
        Box(
            modifier = Modifier
                .background(lineColor)
                .height(1.dp)
                .weight(1f)
        )
        Text(text = "or continue with", color = lineColor.copy(1F), modifier = Modifier.padding(horizontal = 10.dp))
        Box(
            modifier = Modifier
                .background(lineColor)
                .height(1.dp)
                .weight(1f)
        )
    }

    Spacer(modifier = Modifier.height(20.dp))

    Row(
        modifier = Modifier.padding(horizontal = 16.dp)
    ) {
        val iconList = listOf(
            painterResource(id = R.mipmap.wechat),
            painterResource(id = R.mipmap.qq),
            painterResource(id = R.mipmap.weibo)
        )
        iconList.forEach {
            IconButton(
                onClick = { /*TODO*/ },
                modifier = Modifier
                    .padding(horizontal = 16.dp)
                    .height(60.dp)
                    .weight(1F)
                    .border(
                        1.dp,
                        MaterialTheme.colors.surface.copy(alpha = 0.2F),
                        shape = RoundedCornerShape(UIValue.LARGE_RADIUS)
                    )
            ) {
                Image(painter = it, contentDescription = "WeChat", modifier = Modifier.padding(14.dp))
            }
        }
    }

    Spacer(modifier = Modifier.height(30.dp))

    content()

    Spacer(modifier = Modifier.height(30.dp))
}