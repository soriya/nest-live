package com.soriya.nestlive.screen.page

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.LocalContentColor
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowForward
import androidx.compose.material.icons.filled.Search
import androidx.compose.material.icons.outlined.Visibility
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.viewmodel.compose.viewModel
import com.soriya.nestlive.component.CustomDivider
import com.soriya.nestlive.component.CustomImage
import com.soriya.nestlive.component.CustomTag
import com.soriya.nestlive.component.LargeText
import com.soriya.nestlive.component.LargeTitle
import com.soriya.nestlive.component.LiveCard
import com.soriya.nestlive.component.LiveTag
import com.soriya.nestlive.component.MediumText
import com.soriya.nestlive.component.MediumTitle
import com.soriya.nestlive.component.ScreenColumnContent
import com.soriya.nestlive.component.SmallAdditionalText
import com.soriya.nestlive.component.SmallText
import com.soriya.nestlive.component.SmallerText
import com.soriya.nestlive.constant.ServerConstant
import com.soriya.nestlive.constant.UIValue
import com.soriya.nestlive.model.Category
import com.soriya.nestlive.model.Channel
import com.soriya.nestlive.model.Live
import com.soriya.nestlive.ui.theme.LiveRed
import com.soriya.nestlive.viewmodel.CategoryModel
import com.soriya.nestlive.viewmodel.ChannelModel
import com.soriya.nestlive.viewmodel.LiveModel

@Composable
fun HomeScreen(
    toCategories: () -> Unit,
    toChannels: () -> Unit,
    toRecommendLives: () -> Unit,
    toLiveDetail: (Long) -> Unit,
    toCategoryDetail: (Long) -> Unit,
    toChannelDetail: (Long) -> Unit,
    toSearch: () -> Unit
) {
    ScreenColumnContent(
        modifier = Modifier
            .fillMaxSize()
            .verticalScroll(rememberScrollState())
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Center,
            modifier = Modifier
                .height(50.dp)
                .fillMaxWidth()
                .padding(top = UIValue.VERTICAL_PADDING)
                .clip(RoundedCornerShape(UIValue.SMALLER_RADIUS))
                .background(MaterialTheme.colors.surface.copy(0.1F))
                .clickable(onClick = toSearch)
        ) {
            CompositionLocalProvider(LocalContentColor provides Color.Gray) {
                Icon(imageVector = Icons.Default.Search, contentDescription = "Search")
                SmallText(text = "搜索")
            }
        }

        TitleBar(title = "分类", more = toCategories)
        CategoryList(toCategoryDetail)

        TitleBar(title = "订阅频道", more = toChannels)
        ChannelList(toLiveDetail = toLiveDetail, toChannelDetail = toChannelDetail)

        TitleBar(title = "推荐直播", more = toRecommendLives)
        RecommendLiveList(toLiveDetail = toLiveDetail)

        CustomDivider(text = "已经到底了")
    }
}

@Composable
fun TitleBar(
    title: String,
    more: () -> Unit
) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(vertical = UIValue.VERTICAL_PADDING),
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically
    ) {
        LargeTitle(text = title)
        IconButton(onClick = more) {
            Icon(imageVector = Icons.Default.ArrowForward, contentDescription = "More", tint = MaterialTheme.colors.primary)
        }
    }
}

@Composable
private fun CategoryList(
    toCategoryDetail: (Long) -> Unit
) {
    val model: CategoryModel = hiltViewModel()
    val state by model.categoryList.collectAsState()

    LazyRow {
        items(state) {
            CategoryItem(data = it, onClick = toCategoryDetail)
        }
    }
}

@Composable
private fun CategoryItem(data: Category, onClick: (Long) -> Unit) {
    Column(
//        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .padding(end = UIValue.HORIZONTAL_PADDING)
            .clickable {
                onClick(data.id)
            }
    ) {
        CustomImage(
            url = data.cover,
            contentDescription = "Category",
            shape = RoundedCornerShape(UIValue.LARGE_RADIUS),
            modifier = Modifier.size(80.dp, 120.dp)
        )

        MediumText(
            text = data.categoryName,
            modifier = Modifier.padding(vertical = UIValue.VERTICAL_PADDING)
        )

        Row(
            verticalAlignment = Alignment.CenterVertically
        ) {
            Box(modifier = Modifier
                .size(6.dp)
                .clip(CircleShape)
                .background(Color.Red)
            )
            Spacer(modifier = Modifier.width(5.dp))
            SmallerText(text = "${data.viewers} viewers")
        }
    }
}

@Composable
private fun ChannelList(
    toLiveDetail: (Long) -> Unit,
    toChannelDetail: (Long) -> Unit
) {
    val model: ChannelModel = hiltViewModel()
    val state by model.channelList.collectAsState()

    if (state.isNotEmpty()) {
        LazyRow(
            modifier = Modifier.padding(vertical = 5.dp)
        ) {
            items(state) { channel ->
                ChannelItem(data = channel, onClick = { channelId ->
                    if (channel.liveState == 1) toLiveDetail(channelId)
                    else toChannelDetail(channelId)
                })
            }
        }
    } else {
        Box(
            contentAlignment = Alignment.Center,
            modifier = Modifier
                .fillMaxWidth()
                .height(40.dp)
        ) {
            SmallAdditionalText(text = "暂无数据")
        }
    }

}

@Composable
private fun ChannelItem(data: Channel, onClick: (Long) -> Unit) {
    Box(
        contentAlignment = Alignment.TopCenter,
        modifier = Modifier
            .padding(end = 8.dp)
            .clickable { onClick(data.id ?: 0) }
    ) {
        CustomImage(
            url = data.avatar,
            contentDescription = "Channel",
            shape = CircleShape,
            modifier = Modifier
                .size(72.dp)
        )

        if (data.liveState == 1) Box(
            modifier = Modifier.height(80.dp)
        ) {
            Box(modifier = Modifier
                .size(72.dp)
                .border(2.dp, LiveRed, CircleShape))
            LiveTag(modifier = Modifier.align(Alignment.BottomCenter))
        }
    }
}

@Composable
private fun RecommendLiveList(
    toLiveDetail: (Long) -> Unit
) {
    val model: LiveModel = hiltViewModel()
    val state by model.recommendLiveList.collectAsState()

    state.forEach {
        LiveCard(data = it, onClick = toLiveDetail)
    }
}

