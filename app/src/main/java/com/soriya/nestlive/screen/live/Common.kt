package com.soriya.nestlive.screen.live

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.itemsIndexed
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.LocalContentColor
import androidx.compose.material.MaterialTheme
import androidx.compose.material.OutlinedButton
import androidx.compose.material.TextButton
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Search
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.soriya.nestlive.component.CustomEditText
import com.soriya.nestlive.component.MediumText
import com.soriya.nestlive.component.MediumTitle
import com.soriya.nestlive.component.SmallText
import com.soriya.nestlive.constant.UIValue
import com.soriya.nestlive.model.Category
import com.soriya.nestlive.viewmodel.CategoryModel


@Composable
fun Title(
    text: String,
    right: @Composable (() -> Unit)? = null
) {
    Row(
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier
            .fillMaxWidth()
    ) {
        MediumTitle(
            text = text,
            modifier = Modifier
                .padding(vertical = UIValue.VERTICAL_PADDING)
        )

        if (right != null) right()
    }
}

@Composable
fun CategoryModal(
    onSelected: (Category) -> Unit
) = Column(
    horizontalAlignment = Alignment.CenterHorizontally,
    modifier = Modifier
        .padding(vertical = UIValue.VERTICAL_PADDING, horizontal = UIValue.HORIZONTAL_PADDING)
) {
    var categoryIndex by remember {
        mutableStateOf(-1)
    }

    var searchKey by remember {
        mutableStateOf("")
    }

    val categoryModel: CategoryModel = hiltViewModel()
    val categoryList by categoryModel.categoryAll.collectAsState(initial = emptyList())

    Spacer(
        modifier = Modifier
            .size(40.dp, 5.dp)
            .clip(RoundedCornerShape(50))
            .background(MaterialTheme.colors.surface)
    )
    MediumTitle(text = "选择分类", modifier = Modifier.padding(vertical = UIValue.VERTICAL_PADDING.times(2)))
    CustomEditText(value = searchKey, onValueChange = { searchKey = it }, placeholder = "搜索", leftIcon = Icons.Default.Search)
    LazyVerticalGrid(
        columns = GridCells.Fixed(2),
        verticalArrangement = Arrangement.spacedBy(UIValue.VERTICAL_PADDING),
        horizontalArrangement = Arrangement.spacedBy(UIValue.HORIZONTAL_PADDING),
        modifier = Modifier
            .padding(top = UIValue.VERTICAL_PADDING.times(2))
            .heightIn(min = 0.dp, max = 300.dp)
    ) {

        itemsIndexed(
            items = categoryList
        ) { index, item ->
            CategoryButton(
                name = item.categoryName,
                isSelect = index == categoryIndex
            ) {
                categoryIndex = index
                onSelected(item)
            }
        }
    }
}

@Composable
fun TagModal(
    onComplete: (String) -> Unit
) = Column(
    horizontalAlignment = Alignment.CenterHorizontally,
    modifier = Modifier
        .padding(vertical = UIValue.VERTICAL_PADDING, horizontal = UIValue.HORIZONTAL_PADDING)
) {

    val focusRequester = remember {
        FocusRequester()
    }

    var tag by remember {
        mutableStateOf("")
    }

    Spacer(
        modifier = Modifier
            .size(40.dp, 5.dp)
            .clip(RoundedCornerShape(50))
            .background(MaterialTheme.colors.surface)
    )
    Box(
        modifier = Modifier.fillMaxWidth()
    ) {
        MediumTitle(
            text = "添加标签",
            modifier = Modifier
                .padding(vertical = UIValue.VERTICAL_PADDING.times(2))
                .align(Alignment.Center)
        )
        TextButton(
            onClick = {
                onComplete(tag)
                tag = ""
            },
            modifier = Modifier.align(Alignment.CenterEnd)
        ) {
            SmallText(text = "确定")
        }
    }
    CustomEditText(
        value = tag,
        onValueChange = { tag = it },
        modifier = Modifier.focusRequester(focusRequester)
    )

    LaunchedEffect(Unit) {
//        delay(100)
        focusRequester.requestFocus()
    }
}

@Composable
fun TagItem(
    modifier: Modifier = Modifier,
    tag: String,
    key: Int? = null,
    onClose: ((Int?) -> Unit)? = null
) {
    OutlinedButton(
        onClick = { onClose?.invoke(key) },
        shape = RoundedCornerShape(50),
        colors = ButtonDefaults.outlinedButtonColors(
            backgroundColor = MaterialTheme.colors.background
        ),
        border = BorderStroke(1.dp, MaterialTheme.colors.primary),
        modifier = modifier
    ) {
        SmallText(text = tag)
        if (onClose != null) Icon(imageVector = Icons.Default.Close, contentDescription = "Close", modifier = Modifier.size(16.dp))
    }
}

@Composable
fun IconTextButton(
    icon: ImageVector,
    contentDescription: String,
    text: String,
    selector: Boolean = false,
    onclick: () -> Unit
) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        CompositionLocalProvider(LocalContentColor provides if (selector) MaterialTheme.colors.primary else LocalContentColor.current) {
            IconButton(
                onClick = onclick,
                modifier = Modifier
                    .clip(CircleShape)
                    .border(BorderStroke(1.dp, LocalContentColor.current), CircleShape)
                    .background(Color.Black.copy(0.1F))
                    .padding(4.dp)
            ) {
                Icon(imageVector = icon, contentDescription = contentDescription)
            }
            Spacer(modifier = Modifier.height(4.dp))
            SmallText(text = text)
        }
    }
}

@Composable
fun CategoryButton(
    name: String,
    isSelect: Boolean = false,
    onClick: () -> Unit = {}
) {
    Box(
        contentAlignment = Alignment.Center,
        modifier = Modifier
            .height(40.dp)
            .border(
                1.dp,
                if (isSelect) MaterialTheme.colors.primary else MaterialTheme.colors.surface.copy(
                    0.7F
                ),
                RoundedCornerShape(UIValue.SMALL_RADIUS)
            )
            .clickable(onClick = onClick)
    ) {
        MediumText(text = name, color = if (isSelect) MaterialTheme.colors.primary else Color.Unspecified)
    }
}