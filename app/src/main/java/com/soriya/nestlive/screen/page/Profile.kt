package com.soriya.nestlive.screen.page

import android.content.Context
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.Card
import androidx.compose.material.Checkbox
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardArrowRight
import androidx.compose.material.icons.filled.KeyboardCommandKey
import androidx.compose.material.icons.filled.Person
import androidx.compose.material.icons.filled.Settings
import androidx.compose.material.icons.filled.WifiChannel
import androidx.compose.material.icons.filled.Workspaces
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.blur
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import com.soriya.nestlive.component.CustomImage
import com.soriya.nestlive.component.MediumText
import com.soriya.nestlive.component.MediumTitle
import com.soriya.nestlive.component.ScreenColumnContent
import com.soriya.nestlive.component.SmallAdditionalText
import com.soriya.nestlive.component.SmallerAdditionalText
import com.soriya.nestlive.constant.CommonConstant
import com.soriya.nestlive.constant.UIValue
import com.soriya.nestlive.db.entity.UserInfoEntity
import com.soriya.nestlive.router.ScreenPath
import com.soriya.nestlive.ui.theme.HermesOrange
import com.soriya.nestlive.ui.theme.KleinBlue
import com.soriya.nestlive.ui.theme.MarsGreen
import com.soriya.nestlive.viewmodel.UserModel

@Composable
fun ProfileScreen(
    to: (ScreenPath) -> Unit
) {
    val context = LocalContext.current
    val sharedPreferences = context.getSharedPreferences(CommonConstant.USER_INFO_KEY, Context.MODE_PRIVATE)
    val token = sharedPreferences.getString("token", null)

    if (token == null) {
        ScreenColumnContent(
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            MediumTitle(text = "登录即可享受完整版功能")
            Spacer(modifier = Modifier.height(20.dp))
            Button(
                onClick = { to(ScreenPath.SignIn) },
                shape = RoundedCornerShape(50)
            ) {
                MediumText(text = "去登录")
            }
            Spacer(modifier = Modifier.height(20.dp))
            Row(
                verticalAlignment = Alignment.CenterVertically
            ) {
                Checkbox(checked = false, onCheckedChange = {}, modifier = Modifier.size(28.dp))
                SmallerAdditionalText(text = "我已阅读并同意用户协议、隐私政策服务协议")
            }
        }
    } else {
        ScreenColumnContent {
            PersonInfo()
            Column(
                modifier = Modifier
                    .fillMaxSize()
            ) {
                PersonalCenter(to)
                Spacer(modifier = Modifier
                    .fillMaxWidth()
                    .height(1.dp)
                    .background(MaterialTheme.colors.surface.copy(alpha = 0.2F))
                )
                ProfileCenter(to)
            }
        }
    }

}

@Composable
fun PersonInfo() {

    val userModel: UserModel = hiltViewModel()
    val userInfo by userModel.userInfo.collectAsState()

    LaunchedEffect(Unit) {
        userModel.info()
    }

    if (userInfo == null) return

    val height: Dp = 100.dp
    Card(
        backgroundColor = MaterialTheme.colors.background,
        modifier = Modifier
            .padding(vertical = UIValue.VERTICAL_PADDING)
            .fillMaxWidth()
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .height(height)
        ) {
            CustomImage(
                url = userInfo!!.avatar,
                contentDescription = "Avatar",
                shape = CircleShape,
                modifier = Modifier.size(height)
            )

            Spacer(modifier = Modifier.width(20.dp))

            Column(
                modifier = Modifier.fillMaxSize(),
                verticalArrangement = Arrangement.SpaceEvenly
            ) {
                MediumTitle(text = userInfo!!.nickname)
                SmallAdditionalText(text = "Offline")
            }
        }
    }
}

@Composable
fun PersonalCenter(
    toDetail: (ScreenPath) -> Unit
) {
    Column(
        modifier = Modifier
            .padding(vertical = UIValue.VERTICAL_PADDING)
    ) {
        AccountItem(
            title = "我的频道",
            icon = Icons.Default.Workspaces,
        ) {
            toDetail(ScreenPath.MyChannel)
        }

        AccountItem(
            title = "动态",
            icon = Icons.Default.KeyboardCommandKey,
            iconColor = MarsGreen
        ) {}
    }
}

@Composable
fun ProfileCenter(
    toDetail: (ScreenPath) -> Unit
) {
    Column(
        modifier = Modifier
            .padding(vertical = UIValue.VERTICAL_PADDING)
    ) {
        AccountItem(
            title = "个人中心",
            icon = Icons.Default.Person,
            iconColor = HermesOrange
        ) {}

        AccountItem(
            title = "设置",
            icon = Icons.Default.Settings,
            iconColor = KleinBlue
        ) {
            toDetail(ScreenPath.Setting)
        }
    }
}

@Composable
fun AccountItem(
    modifier: Modifier = Modifier,
    title: String,
    icon: ImageVector,
    iconColor: Color = MaterialTheme.colors.primary,
    onClick: () -> Unit
) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.SpaceBetween,
        modifier = modifier
            .clickable(onClick = onClick)
            .fillMaxWidth()
            .padding(vertical = UIValue.VERTICAL_PADDING)
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically
        ) {
            Box(
                modifier = Modifier
                    .size(50.dp)
                    .clip(CircleShape)
                    .background(iconColor.copy(alpha = 0.1F)),
                contentAlignment = Alignment.Center
            ) {
                Icon(imageVector = icon, contentDescription = title, tint = iconColor)
            }

            Spacer(modifier = Modifier.width(20.dp))

            MediumTitle(text = title)
        }

        Icon(imageVector = Icons.Default.KeyboardArrowRight, contentDescription = "To")
    }
}