package com.soriya.nestlive.screen.profile

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.AlertDialog
import androidx.compose.material.Icon
import androidx.compose.material.LocalContentColor
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowForwardIos
import androidx.compose.material.icons.filled.Tag
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawWithContent
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.hilt.navigation.compose.hiltViewModel
import com.soriya.nestlive.component.MediumText
import com.soriya.nestlive.component.ScreenColumnContent
import com.soriya.nestlive.constant.UIValue
import com.soriya.nestlive.viewmodel.ChannelModel
import com.soriya.nestlive.viewmodel.UserModel
import kotlinx.coroutines.launch

@Composable
fun SettingScreen(
    back: () -> Unit
) = ScreenColumnContent(
    modifier = Modifier.padding(vertical = UIValue.VERTICAL_PADDING)
) {
    var openLogoutDialog by remember {
        mutableStateOf(false)
    }

    val userModel: UserModel = hiltViewModel()
    val channelModel: ChannelModel = hiltViewModel()

    val coroutineScope = rememberCoroutineScope()

    SettingItem(
        icon = Icons.Default.Tag,
        text = "退出登录",
        to = {
            openLogoutDialog = true
        }
    )

    takeIf {
        openLogoutDialog
    }?.let {
        AlertDialog(
            onDismissRequest = {
                openLogoutDialog = false
            },
            confirmButton = {
                TextButton(onClick = {
                    openLogoutDialog = false
                    coroutineScope.launch {
                        userModel.logout().collect {
                            back()
                        }
                    }

                }) {
                    Text(text = "确认")
                }
            },
            dismissButton = {
                TextButton(onClick = {
                    openLogoutDialog = false
                }) {
                    Text(text = "取消")
                }
            },
            text = {
                MediumText(text = "确定退出？")
            },
            backgroundColor = MaterialTheme.colors.background,
            modifier = Modifier.background(Color.Black)
        )
    }
}

@Composable
private fun SettingItem(
    icon: ImageVector,
    text: String,
    to: () -> Unit
) = ConstraintLayout(
    modifier = Modifier
        .fillMaxWidth()
        .clickable(onClick = to)
        .padding(vertical = UIValue.VERTICAL_PADDING.times(2))
        .drawWithContent {
            drawContent()
        }
) {
    val (iconRef, textRef, toRef) = remember {
        createRefs()
    }
    Icon(
        imageVector = icon,
        contentDescription = "Test",
        modifier = Modifier
            .constrainAs(iconRef) {
                start.linkTo(parent.start)
                top.linkTo(parent.top)
                bottom.linkTo(parent.bottom)
            }
    )
    MediumText(
        text = text,
        modifier = Modifier
            .constrainAs(textRef) {
                start.linkTo(iconRef.end)
                top.linkTo(parent.top)
                bottom.linkTo(parent.bottom)
            }
            .padding(start = UIValue.HORIZONTAL_PADDING)
    )
    CompositionLocalProvider(LocalContentColor provides MaterialTheme.colors.surface) {
        Icon(
            imageVector = Icons.Default.ArrowForwardIos,
            contentDescription = "To",
            modifier = Modifier
                .constrainAs(toRef) {
                    end.linkTo(parent.end)
                    top.linkTo(parent.top)
                    bottom.linkTo(parent.bottom)
                }
                .size(16.dp)
        )
    }
}
