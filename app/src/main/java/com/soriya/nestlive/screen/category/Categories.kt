package com.soriya.nestlive.screen.category

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.paging.compose.collectAsLazyPagingItems
import androidx.paging.compose.itemKey
import com.soriya.nestlive.component.CustomImage
import com.soriya.nestlive.component.CustomTag
import com.soriya.nestlive.component.ScreenColumnContent
import com.soriya.nestlive.constant.UIValue
import com.soriya.nestlive.model.Category
import com.soriya.nestlive.viewmodel.CategoryModel

@Composable
fun CategoriesScreen(
    toCategoryDetail: (Long) -> Unit
) {
    val model: CategoryModel = hiltViewModel()
    val items = model.categoryListFlow.collectAsLazyPagingItems()

    ScreenColumnContent {
        LazyColumn {
            items(
                items.itemCount,
                key = items.itemKey { it.id }
            ) {
                val item = items[it]
                CategoryItem(item!!, toCategoryDetail)
            }
        }
    }
}

@Composable
private fun CategoryItem(
    data: Category,
    toCategoryDetail: (Long) -> Unit
) {
    val height = 150.dp

    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(vertical = UIValue.VERTICAL_PADDING)
            .height(height)
            .clickable {
                toCategoryDetail(data.id)
            }
    ) {
        CustomImage(
            url = data.cover,
            contentDescription = data.categoryName,
            shape = RoundedCornerShape(UIValue.LARGE_RADIUS),
            modifier = Modifier.size(100.dp, height)
        )

        Spacer(modifier = Modifier.width(UIValue.HORIZONTAL_PADDING))

        Column(
            verticalArrangement = Arrangement.SpaceBetween,
            modifier = Modifier
                .fillMaxSize()
                .padding(vertical = UIValue.VERTICAL_PADDING.times(2))
        ) {
            Text(text = data.categoryName, fontWeight = FontWeight.Bold, fontSize = 22.sp, maxLines = 1, overflow = TextOverflow.Ellipsis)
            Text(text = "${data.viewers} Viewers", color = MaterialTheme.colors.surface)

            Row {
                data.tags.split("|").forEach {
                    CustomTag(tag = it)
                }
            }
        }
    }
}