package com.soriya.nestlive.constant

import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp

object UIValue {

    val HORIZONTAL_PADDING: Dp = 16.dp

    val VERTICAL_PADDING: Dp = 10.dp

    // ---

    val LARGE_RADIUS: Dp = 20.dp

    val MEDIUM_RADIUS: Dp = 16.dp

    val SMALL_RADIUS: Dp = 10.dp

    val SMALLER_RADIUS: Dp = 6.dp

}