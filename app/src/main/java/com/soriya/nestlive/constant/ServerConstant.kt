package com.soriya.nestlive.constant

object ServerConstant {

//    private const val BASE_IP = "120.79.90.109"
    private const val BASE_IP = "172.20.10.4"
//    private const val BASE_IP = "192.168.10.108"
//    private const val BASE_IP = "192.168.24.1"

    const val RTMP_URL = "rtmp://$BASE_IP/live/"

    const val SERVER_URL = "http://$BASE_IP:8088"

    const val TCP_HOST = BASE_IP

    const val TCP_PORT = 8000

    const val IMAGE_URL = "$SERVER_URL/static"

    const val VOD_URL = "$SERVER_URL/static"

}