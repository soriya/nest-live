package com.soriya.nestlive.constant

import androidx.lifecycle.LiveData
import java.lang.Exception

sealed class ResponseState {
    data class Success<T>(val data: T): ResponseState()

    data class SuccessList<T>(val data: List<T>): ResponseState()

    data class Error(val exception: Throwable): ResponseState()
}
