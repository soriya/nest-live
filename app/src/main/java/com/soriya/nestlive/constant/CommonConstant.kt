package com.soriya.nestlive.constant

object CommonConstant {

    const val USER_INFO_KEY = "UserInfo"

    const val STREAMING_CONFIG_KEY = "StreamingConfig"

}