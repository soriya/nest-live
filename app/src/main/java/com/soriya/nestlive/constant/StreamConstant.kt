package com.soriya.nestlive.constant

object StreamConstant {

    /**
     * 视频推流TYPE
     */
    const val RTMP_PUSH_VIDEO = 0

    /**
     * 音频解码数据推流TYPE
     */
    const val RTMP_PUSH_AUDIO_DECODE = 1

    /**
     * 音频数据推流TYPE
     */
    const val RTMP_PUSH_AUDIO_DATA = 2

    /**
     * 屏幕方向竖屏
     */
    const val SCREEN_VERTICAL = 101

    /**
     * 屏幕方向横屏
     */
    const val SCREEN_HORIZONTAL = 102

    /**
     * 默认帧率
     */
    const val DEFAULT_FRAME_RATE = 15

    /**
     * 默认码率(清晰度) 0.03标清 0.1高清 0.4超清
     */
    const val DEFAULT_BIT_RATE = 0.03

}