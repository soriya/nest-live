package com.soriya.nestlive.component

import android.util.Log
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.OutlinedButton
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.CheckCircle
import androidx.compose.material.icons.filled.MoreVert
import androidx.compose.material.icons.outlined.Visibility
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.drawWithContent
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.ConstraintLayoutScope
import com.soriya.nestlive.constant.UIValue
import com.soriya.nestlive.model.Channel
import com.soriya.nestlive.model.Live
import com.soriya.nestlive.model.Video
import com.soriya.nestlive.ui.theme.LiveRed
import com.soriya.nestlive.util.DateUtil

@Composable
fun ScreenBoxContent(
    modifier: Modifier = Modifier,
    content: @Composable BoxScope.() -> Unit
) {
    Box(
        modifier = modifier
            .fillMaxSize()
            .padding(horizontal = UIValue.HORIZONTAL_PADDING),
        content = content
    )
}

@Composable
fun ScreenRowContent(
    modifier: Modifier = Modifier,
    content: @Composable RowScope.() -> Unit
) {
    Row(
        modifier = modifier
            .fillMaxSize()
            .padding(horizontal = UIValue.HORIZONTAL_PADDING),
        content = content
    )
}

@Composable
fun ScreenColumnContent(
    modifier: Modifier = Modifier,
    verticalArrangement: Arrangement.Vertical = Arrangement.Top,
    horizontalAlignment: Alignment.Horizontal = Alignment.Start,
    content: @Composable ColumnScope.() -> Unit
) {
    Column(
        verticalArrangement = verticalArrangement,
        horizontalAlignment = horizontalAlignment,
        modifier = modifier
            .fillMaxSize()
            .padding(horizontal = UIValue.HORIZONTAL_PADDING),
        content = content
    )
}

@Composable
fun ScreenConstraintContent(
    modifier: Modifier = Modifier,
    content: @Composable ConstraintLayoutScope.() -> Unit
) {
    ConstraintLayout(
        modifier = modifier
            .fillMaxSize()
            .padding(horizontal = UIValue.HORIZONTAL_PADDING),
        content = content
    )
}

@Composable
fun CustomTag(tag: String) {
    Text(
        text = tag,
        fontSize = 12.sp,
        color = MaterialTheme.colors.surface,
        modifier = Modifier
            .padding(end = 6.dp)
            .border(1.dp, MaterialTheme.colors.surface, RoundedCornerShape(8.dp))
            .padding(horizontal = 6.dp, vertical = 3.dp),
    )
}

@Composable
fun NavMenu(
    content: @Composable RowScope.() -> Unit
) {
    Row(
        horizontalArrangement = Arrangement.SpaceBetween,
        modifier = Modifier
            .fillMaxWidth(),
        content = content
    )
}


@Composable
fun RowScope.NavMenuItem(
    modifier: Modifier = Modifier,
    title: String,
    select: Boolean = false,
    size: Int = 1,
    onClick: () -> Unit
) {
    val color = if (select) MaterialTheme.colors.primary else MaterialTheme.colors.surface
    val lineWidth = 3.dp
    Box(
        contentAlignment = Alignment.Center,
        modifier = modifier
            .padding(vertical = UIValue.VERTICAL_PADDING)
            .clickable { onClick() }
            .drawWithContent {
                drawContent()
                if (select)
                    drawRect(
                        color = color,
                        topLeft = Offset(0F, this.size.height - lineWidth.toPx()),
                        size = Size(this.size.width, lineWidth.toPx())
                    )
            }
            .padding(horizontal = UIValue.HORIZONTAL_PADDING, vertical = 10.dp)
            .weight(1F)
    ) {
        when (size) {
            1 -> LargerText(text = title, color = color)
            2 -> LargeText(text = title, color = color)
            3 -> MediumText(text = title, color = color)
            4 -> SmallText(text = title, color = color)
        }
    }
}

@Composable
fun LiveTag(modifier: Modifier = Modifier) = SmallText(
    text = "Live",
    color = Color.White,
    textAlign = TextAlign.Center,
    modifier = modifier
        .clip(RoundedCornerShape(UIValue.SMALLER_RADIUS))
        .background(LiveRed)
        .padding(horizontal = 4.dp, vertical = 2.dp)
)

@Composable
fun LiveCard(data: Live, onClick: (Long) -> Unit) {
    Row(
        modifier = Modifier
            .padding(bottom = UIValue.VERTICAL_PADDING.times(2))
            .fillMaxWidth()
            .height(90.dp)
            .clip(RoundedCornerShape(UIValue.LARGE_RADIUS))
            .clickable { onClick(data.channelId) }
    ) {
        Box {
            CustomImage(
                url = data.cover,
                contentDescription = "Live",
                shape = RoundedCornerShape(UIValue.LARGE_RADIUS),
                modifier = Modifier
                    .width(160.dp)
                    .fillMaxHeight()
            )

            Row(
                modifier = Modifier
                    .align(Alignment.BottomStart)
                    .padding(start = 5.dp, bottom = 5.dp),
                verticalAlignment = Alignment.CenterVertically
            ) {
                val color = MaterialTheme.colors.background
                Icon(imageVector = Icons.Outlined.Visibility, contentDescription = "Views", tint = color, modifier = Modifier.size(18.dp))
                SmallText(text = "2.4k", color = color)
            }
        }

        Spacer(modifier = Modifier.width(20.dp))

        Column(
            modifier = Modifier
                .fillMaxHeight()
                .weight(1F),
            verticalArrangement = Arrangement.SpaceBetween
        ) {
            Row {
                data.avatar.let {
                    CustomImage(
                        url = it,
                        contentDescription = "Avatar",
                        shape = CircleShape,
                        modifier = Modifier.size(26.dp)
                    )
                    Spacer(modifier = Modifier.width(10.dp))
                }
                MediumTitle(text = data.nickname)
            }

            SmallText(text = data.liveDesc, color = MaterialTheme.colors.surface, maxLines = 1/* , overflow = TextOverflow.Ellipsis */)

            Row {
                data.tags.split("|").forEach {
                    CustomTag(tag = it)
                }
            }
        }
        
        Icon(imageVector = Icons.Default.MoreVert, contentDescription = "More Operate", tint = MaterialTheme.colors.surface)
    }
}

@Composable
fun VideoCard(
    data: Video,
    onClick: () -> Unit
) {
    Row(
        modifier = Modifier
            .padding(bottom = UIValue.VERTICAL_PADDING)
            .fillMaxWidth()
            .height(100.dp)
            .clip(RoundedCornerShape(UIValue.LARGE_RADIUS))
            .clickable(onClick = onClick)
    ) {
        Box {
            CustomImage(
                url = data.cover,
                contentDescription = "Video Cover",
                shape = RoundedCornerShape(UIValue.LARGE_RADIUS),
                modifier = Modifier
                    .width(180.dp)
                    .fillMaxHeight()
            )

            SmallerText(
                text = DateUtil.secondToTimeStr(data.duration),
                color = Color.White,
                modifier = Modifier
                    .align(Alignment.TopStart)
                    .padding(start = 10.dp, top = 10.dp)
                    .clip(RoundedCornerShape(UIValue.SMALLER_RADIUS))
                    .background(MaterialTheme.colors.surface.copy(alpha = 0.8F))
                    .padding(horizontal = 6.dp, vertical = 4.dp)
            )
        }

        Spacer(modifier = Modifier.width(20.dp))

        Column(
            modifier = Modifier
                .fillMaxHeight()
                .padding(vertical = UIValue.VERTICAL_PADDING),
            verticalArrangement = Arrangement.SpaceBetween
        ) {
            MediumTitle(text = data.videoName)

            Row(
                verticalAlignment = Alignment.CenterVertically
            ) {
                SmallText(text = data.nickname)
                Spacer(modifier = Modifier.width(6.dp))
                Icon(
                    imageVector = Icons.Default.CheckCircle,
                    contentDescription = "Icon",
                    tint = MaterialTheme.colors.primary,
                    modifier = Modifier.size(18.dp)
                )
            }

            SmallerText(text = "${data.viewers} Views · 1 Hour ago")
        }
    }
}

@Composable
fun CustomDivider(text: String) = Row(
    verticalAlignment = Alignment.CenterVertically,
    modifier = Modifier.padding(vertical = UIValue.VERTICAL_PADDING)
) {
    val lineColor = MaterialTheme.colors.surface.copy(0.1F)

    Spacer(modifier = Modifier
        .height(1.dp)
        .weight(1F)
        .background(lineColor))
    if (text.isNotEmpty()) SmallAdditionalText(text = text, modifier = Modifier.padding(horizontal = 5.dp))
    Spacer(modifier = Modifier
        .height(1.dp)
        .weight(1F)
        .background(lineColor))
}

@Composable
fun ChannelItem(
    data: Channel,
    isFollow: Boolean = true,
    toChannelDetail: (Long) -> Unit
) {
    val height = 80.dp

    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier
            .padding(vertical = UIValue.VERTICAL_PADDING)
            .clickable { toChannelDetail(data.id!!) }
    ) {
        CustomImage(
            url = data.avatar,
            contentDescription = "Cover",
            shape = CircleShape,
            modifier = Modifier.size(height)
        )

        Column(
            verticalArrangement = Arrangement.SpaceBetween,
            modifier = Modifier
                .padding(start = 20.dp)
                .weight(1F)
        ) {
            SmallTitle(text = data.channelName)
            Spacer(modifier = Modifier.height(UIValue.VERTICAL_PADDING))
            SmallText(text = "${data.follows} Followers")
        }

        if (isFollow) OutlinedButton(
            onClick = { /*TODO*/ },
            shape = RoundedCornerShape(50),
            colors = ButtonDefaults.outlinedButtonColors(backgroundColor = MaterialTheme.colors.background),
            border = BorderStroke(2.dp, MaterialTheme.colors.primary)
        ) {
            MediumText(text = "取消订阅")
        } else Button(
            onClick = { /*TODO*/ },
            shape = RoundedCornerShape(50)
        ) {
            MediumText(text = "订阅")
        }
    }
}