package com.soriya.nestlive.component

import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.sp

@Composable
private fun BaseText(
    modifier: Modifier = Modifier,
    text: String,
    fontSize: TextUnit,
    color: Color = Color.Unspecified,
    maxLines: Int = Int.MAX_VALUE,
    minLines: Int = 1,
    overflow: TextOverflow = TextOverflow.Ellipsis,
    lineHeight: TextUnit = TextUnit.Unspecified,
    textAlign: TextAlign? = null,
    fontWeight: FontWeight? = null,
) = Text(text = text, fontSize = fontSize, color = color, maxLines = maxLines, minLines = minLines, overflow = overflow, lineHeight = lineHeight, textAlign = textAlign, fontWeight = fontWeight, modifier = modifier)

@Composable
fun LargeTitle(
    modifier: Modifier = Modifier,
    text: String,
    color: Color = Color.Unspecified,
    maxLines: Int = Int.MAX_VALUE,
    minLines: Int = 1,
    overflow: TextOverflow = TextOverflow.Ellipsis,
    lineHeight: TextUnit = TextUnit.Unspecified,
    textAlign: TextAlign? = null
) = BaseText(
    text = text,
    fontWeight = FontWeight.Bold,
    fontSize = 22.sp,
    color = color,
    maxLines = maxLines,
    minLines = minLines,
    overflow = overflow,
    lineHeight = lineHeight,
    textAlign = textAlign,
    modifier = modifier
)

@Composable
fun MediumTitle(
    modifier: Modifier = Modifier,
    text: String,
    color: Color = Color.Unspecified,
    maxLines: Int = Int.MAX_VALUE,
    minLines: Int = 1,
    overflow: TextOverflow = TextOverflow.Ellipsis,
    lineHeight: TextUnit = TextUnit.Unspecified,
    textAlign: TextAlign? = null
) = BaseText(
    text = text,
    fontWeight = FontWeight.Bold,
    fontSize = 20.sp,
    color = color,
    maxLines = maxLines,
    minLines = minLines,
    overflow = overflow,
    lineHeight = lineHeight,
    textAlign = textAlign,
    modifier = modifier
)

@Composable
fun SmallTitle(
    modifier: Modifier = Modifier,
    text: String,
    color: Color = Color.Unspecified
) = BaseText(
    text = text,
    fontWeight = FontWeight.Bold,
    fontSize = 18.sp,
    color = color,
    modifier = modifier
)

// ---

@Composable
fun LargeAdditionalText(modifier: Modifier = Modifier, text: String, color: Color = MaterialTheme.colors.surface) =
    BaseText(text = text, fontSize = 18.sp, color = color, modifier = modifier)

@Composable
fun MediumAdditionalText(modifier: Modifier = Modifier, text: String, color: Color = MaterialTheme.colors.surface) =
    BaseText(text = text, fontSize = 16.sp, color = color, modifier = modifier)

@Composable
fun SmallAdditionalText(modifier: Modifier = Modifier, text: String, color: Color = MaterialTheme.colors.surface) =
    BaseText(text = text, fontSize = 14.sp, color = color, modifier = modifier)

@Composable
fun SmallerAdditionalText(modifier: Modifier = Modifier, text: String, color: Color = MaterialTheme.colors.surface) =
    BaseText(text = text, fontSize = 12.sp, color = color, modifier = modifier)

// ---

@Composable
fun LargerText(
    modifier: Modifier = Modifier,
    text: String,
    color: Color = Color.Unspecified,
    maxLines: Int = Int.MAX_VALUE,
    minLines: Int = 1,
    lineHeight: TextUnit = TextUnit.Unspecified,
    textAlign: TextAlign? = null
) = BaseText(text = text, fontSize = 20.sp, color = color, maxLines = maxLines, minLines = minLines, lineHeight = lineHeight, textAlign = textAlign, modifier = modifier)

@Composable
fun LargeText(
    modifier: Modifier = Modifier,
    text: String,
    color: Color = Color.Unspecified,
    maxLines: Int = Int.MAX_VALUE,
    minLines: Int = 1,
    lineHeight: TextUnit = TextUnit.Unspecified,
    textAlign: TextAlign? = null
) = BaseText(text = text, fontSize = 18.sp, color = color, maxLines = maxLines, minLines = minLines, lineHeight = lineHeight, textAlign = textAlign, modifier = modifier)

@Composable
fun MediumText(
    modifier: Modifier = Modifier,
    text: String,
    color: Color = Color.Unspecified,
    maxLines: Int = Int.MAX_VALUE,
    minLines: Int = 1,
    lineHeight: TextUnit = TextUnit.Unspecified,
    textAlign: TextAlign? = null
) = BaseText(text = text, fontSize = 16.sp, color = color, maxLines = maxLines, minLines = minLines, lineHeight = lineHeight, textAlign = textAlign, modifier = modifier)

@Composable
fun SmallText(
    modifier: Modifier = Modifier,
    text: String,
    color: Color = Color.Unspecified,
    maxLines: Int = Int.MAX_VALUE,
    minLines: Int = 1,
    lineHeight: TextUnit = TextUnit.Unspecified,
    textAlign: TextAlign? = null
) = BaseText(text = text, fontSize = 14.sp, color = color, maxLines = maxLines, minLines = minLines, lineHeight = lineHeight, textAlign = textAlign, modifier = modifier)

@Composable
fun SmallerText(
    modifier: Modifier = Modifier,
    text: String,
    color: Color = Color.Unspecified,
    maxLines: Int = Int.MAX_VALUE,
    minLines: Int = 1,
    overflow: TextOverflow = TextOverflow.Ellipsis,
    lineHeight: TextUnit = TextUnit.Unspecified,
) = BaseText(text = text, fontSize = 12.sp, color = color, maxLines = maxLines, minLines = minLines, lineHeight = lineHeight, overflow = overflow, modifier = modifier)