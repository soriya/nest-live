package com.soriya.nestlive.component

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.focusable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.defaultMinSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.shape.ZeroCornerSize
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Icon
import androidx.compose.material.LocalTextStyle
import androidx.compose.material.MaterialTheme
import androidx.compose.material.OutlinedButton
import androidx.compose.material.Text
import androidx.compose.material.TextFieldColors
import androidx.compose.material.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.unit.dp
import com.soriya.nestlive.constant.UIValue

@Composable
fun CustomEditText(
    value: String,
    onValueChange: (String) -> Unit,
    modifier: Modifier = Modifier,
    enabled: Boolean = true,
    leftIcon: ImageVector? = null,
    rightIcon: ImageVector? = null,
    rightIconOnClick: () -> Unit = {},
    placeholder: String? = null,
    textStyle: TextStyle = LocalTextStyle.current,
    visualTransformation: VisualTransformation = VisualTransformation.None,
    keyboardOptions: KeyboardOptions = KeyboardOptions.Default,
    keyboardActions: KeyboardActions = KeyboardActions(),
    colors: TextFieldColors = TextFieldDefaults.textFieldColors(
        backgroundColor = MaterialTheme.colors.surface.copy(alpha = 0.04F)
    ),
    shape: Shape = RoundedCornerShape(UIValue.LARGE_RADIUS)
) {

    var isFocus by remember {
        mutableStateOf(false)
    }

    BasicTextField(
        value = value,
        onValueChange = onValueChange,
        modifier = modifier
            .border(
                (1.5).dp,
                MaterialTheme.colors.primary.copy(alpha = if (isFocus) 1F else 0F),
                shape
            )
            .background(
                if (isFocus) MaterialTheme.colors.primary.copy(alpha = 0.04F) else colors.backgroundColor(
                    enabled
                ).value,
                shape
            )
            .onFocusChanged { isFocus = it.hasFocus }
            .defaultMinSize(
                minWidth = TextFieldDefaults.MinWidth,
                minHeight = TextFieldDefaults.MinHeight
            ),
        textStyle = textStyle,
        visualTransformation = visualTransformation,
        keyboardOptions = keyboardOptions,
        keyboardActions = keyboardActions,
        singleLine = true,
        maxLines = 1,
        minLines = 1,
        decorationBox = @Composable { innerTextField ->
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(
                        horizontal = UIValue.HORIZONTAL_PADDING.div(2),
                        vertical = UIValue.VERTICAL_PADDING.div(2)
                    ),
                horizontalArrangement = Arrangement.Start,
                verticalAlignment = Alignment.CenterVertically
            ) {
                if (leftIcon != null) {
                    Icon(
                        imageVector = leftIcon,
                        contentDescription = "left icon",
                        tint = if (isFocus) MaterialTheme.colors.primary else MaterialTheme.colors.surface
                    )
                    Spacer(modifier = Modifier.width(10.dp))
                }

                Box(
                    Modifier.weight(1F)
                ) {
                    if (value.isEmpty()) Text(text = placeholder ?: "请输入", color = MaterialTheme.colors.surface)
                    innerTextField()
                }

                if (rightIcon != null) {
                    Spacer(modifier = Modifier.width(10.dp))
                    Icon(
                        imageVector = rightIcon,
                        contentDescription = "right icon",
                        tint = if (isFocus) MaterialTheme.colors.primary else MaterialTheme.colors.surface,
                        modifier = Modifier.clickable(onClick = rightIconOnClick)
                    )
                }
            }
        }
    )
}

@Composable
fun CustomTextArea(
    value: String,
    onValueChange: (String) -> Unit,
    modifier: Modifier = Modifier,
    enabled: Boolean = true,
    placeholder: String? = null,
    textStyle: TextStyle = LocalTextStyle.current,
    visualTransformation: VisualTransformation = VisualTransformation.None,
    keyboardOptions: KeyboardOptions = KeyboardOptions.Default,
    keyboardActions: KeyboardActions = KeyboardActions(),
    maxLines: Int = 3,
    minLines: Int = 2,
    maxLength: Int = 200,
    colors: TextFieldColors = TextFieldDefaults.textFieldColors(
        backgroundColor = MaterialTheme.colors.surface.copy(alpha = 0.04F)
    ),
    shape: Shape = RoundedCornerShape(UIValue.LARGE_RADIUS)
) {

    var isFocus by remember {
        mutableStateOf(false)
    }

    BasicTextField(
        value = value,
        onValueChange = onValueChange,
        modifier = modifier
            .border(
                (1.5).dp,
                MaterialTheme.colors.primary.copy(alpha = if (isFocus) 1F else 0F),
                shape
            )
            .background(
                if (isFocus) MaterialTheme.colors.primary.copy(alpha = 0.04F) else colors.backgroundColor(
                    enabled
                ).value,
                shape
            )
            .onFocusChanged { isFocus = it.hasFocus }
            .defaultMinSize(
                minWidth = TextFieldDefaults.MinWidth,
                minHeight = TextFieldDefaults.MinHeight
            ),
        textStyle = textStyle,
        visualTransformation = visualTransformation,
        keyboardOptions = keyboardOptions,
        keyboardActions = keyboardActions,
        singleLine = false,
        maxLines = maxLines,
        minLines = minLines,
        decorationBox = @Composable { innerTextField ->
            Box(
                modifier
                    .fillMaxWidth(1F)
                    .padding(
                        horizontal = UIValue.HORIZONTAL_PADDING.div(2),
                        vertical = UIValue.VERTICAL_PADDING.div(2)
                    )
            ) {
                if (value.isEmpty()) Text(text = placeholder ?: "请输入", color = MaterialTheme.colors.surface)
                innerTextField()

                SmallerAdditionalText(
                    text = "- ${value.length}/$maxLength -",
                    modifier = Modifier.align(Alignment.BottomEnd)
                )
            }
        }
    )
}

@Composable
fun CustomOutlinedButton(
    modifier: Modifier = Modifier,
    text: String,
    onClick: () -> Unit,
    border: BorderStroke = BorderStroke(1.dp, MaterialTheme.colors.primary)
) {
    CustomOutlinedButton(modifier = modifier, onClick = onClick, border = border) {
        MediumText(text = text)
    }
}

@Composable
fun CustomOutlinedButton(
    modifier: Modifier = Modifier,
    onClick: () -> Unit,
    border: BorderStroke = BorderStroke(1.dp, MaterialTheme.colors.primary),
    content: @Composable RowScope.() -> Unit
) {
    OutlinedButton(
        modifier = modifier,
        onClick = onClick,
        shape = RoundedCornerShape(50),
        colors = ButtonDefaults.outlinedButtonColors(backgroundColor = MaterialTheme.colors.background),
        border = border,
        content = content
    )
}