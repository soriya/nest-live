package com.soriya.nestlive.model

data class Live(
    val id: Long? = null,

    val channelId: Long,

    val categoryId: Long,

    val liveName: String,

    val liveDesc: String,

    val cover: String,

    val tags: String,

    val orientation: Int,

    val viewers: Int,

    val startTime: String,

    val duration: Float,

    val nickname: String = "",

    val avatar: String = ""
) {
    companion object {
        val EMPTY = Live(null, 0,0, "", "", "", "", 0, 0, "", 0F)
    }
}
