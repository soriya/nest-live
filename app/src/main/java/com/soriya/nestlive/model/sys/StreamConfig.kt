package com.soriya.nestlive.model.sys

import com.soriya.nestlive.constant.StreamConstant

data class StreamConfig(
    var frameRate: Int = StreamConstant.DEFAULT_FRAME_RATE,
    var orientation: Int = StreamConstant.SCREEN_HORIZONTAL,
    var bitRate: Double = StreamConstant.DEFAULT_BIT_RATE
)