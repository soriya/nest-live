package com.soriya.nestlive.model

import java.util.Date

data class User(
    val id: Long,

    val account: String,

    val password: String,

    val salt: String,

    val nickname: String,

    val avatar: String,

    val description: String,

    val gender: Int,

    val birth: Date
)