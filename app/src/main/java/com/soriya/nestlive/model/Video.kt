package com.soriya.nestlive.model

data class Video(
    val id: Long,

    val categoryId: Long,

    val videoName: String,

    val videoDesc: String,

    val source: String,

    val path: String?,

    val cover: String,

    val duration: Int,

    val viewers: Int,

    val tags: String?,

    val transFlag: Int,

    val avatar: String,

    val nickname: String,

    val createTime: String
) {
    companion object {
        val EMPTY = Video(0, 0, "", "", "", null, "", 0, 0, null, 0, "", "", "")
    }
}