package com.soriya.nestlive.model

data class PageResult<T>(
    val totalCount: Int,
    val pageSize: Int,
    val totalPage: Int,
    val currentPage: Int,
    val list: List<T>
)