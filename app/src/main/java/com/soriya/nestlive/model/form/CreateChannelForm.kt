package com.soriya.nestlive.model.form

import java.io.File

data class CreateChannelForm(
    val backgroundCover: File,
    val channelName: String,
    val about: String
)