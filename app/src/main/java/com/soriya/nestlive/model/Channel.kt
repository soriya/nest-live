package com.soriya.nestlive.model

data class Channel(
    val id: Long? = null,

    val userId: Long = 0,

    val channelName: String,

    val backgroundCover: String,

    val about: String = "",

    val follows: Int = 0,

    val liveState: Int = 0,

    val nickname: String = "",

    val avatar: String = "",

    val isFollow: Int = 0
) {
    companion object {
        val EMPTY: Channel = Channel(channelName = "", backgroundCover = "")
    }
}