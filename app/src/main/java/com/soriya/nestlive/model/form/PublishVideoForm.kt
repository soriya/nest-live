package com.soriya.nestlive.model.form

import java.io.File

data class PublishVideoForm(
    val channelId: Long,
    val categoryId: Long,
    val videoName: String,
    val videoDesc: String,
    val source: File,
    val cover: ByteArray,
    val duration: Long,
    val tags: String
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as PublishVideoForm

        if (channelId != other.channelId) return false
        if (categoryId != other.categoryId) return false
        if (videoName != other.videoName) return false
        if (videoDesc != other.videoDesc) return false
        if (source != other.source) return false
        if (!cover.contentEquals(other.cover)) return false
        if (duration != other.duration) return false
        if (tags != other.tags) return false

        return true
    }

    override fun hashCode(): Int {
        var result = channelId.hashCode()
        result = 31 * result + categoryId.hashCode()
        result = 31 * result + videoName.hashCode()
        result = 31 * result + videoDesc.hashCode()
        result = 31 * result + source.hashCode()
        result = 31 * result + cover.contentHashCode()
        result = 31 * result + duration.hashCode()
        result = 31 * result + tags.hashCode()
        return result
    }

}
