package com.soriya.nestlive.model

sealed class ResponseResult {

    object Loading: ResponseResult()

    object Failed: ResponseResult()

    data class Success<T>(val code: Int, val message: String, val data: T): ResponseResult()

}