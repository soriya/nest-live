package com.soriya.nestlive.model

import com.soriya.nestlive.im.MessageType

data class LiveChat(
    val id: Long,
    val userId: Long,
    val nickname: String,
    val liveId: Long,
    val content: String,
    val type: MessageType
)