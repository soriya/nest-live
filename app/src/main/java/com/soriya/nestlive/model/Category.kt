package com.soriya.nestlive.model

data class Category(

    val id: Long,

    val categoryName: String,

    val cover: String,

    val followers: Int,

    val viewers: Int,

    val tags: String
) {
    companion object {
        val EMPTY: Category = Category(0, "", "", 0, 0, "")
    }
}