package com.soriya.nestlive.util

object DateUtil {

    fun secondToTimeStr(second: Int): String {
        val h = second / 3600
        val m = (second % 3600) / 60
        val s = second % 60

        return if (h == 0) "${if (m < 10) "0$m" else m}:${if (s < 10) "0$s" else s}"
            else "${if (h < 10) "0$h" else h}:${if (m < 10) "0$m" else m}:${if (s < 10) "0$s" else s}"
    }

}