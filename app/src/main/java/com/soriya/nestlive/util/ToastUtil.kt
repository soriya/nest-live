package com.soriya.nestlive.util

import android.content.Context
import android.os.Looper
import android.widget.Toast

object ToastUtil {

    private var toast: Toast? = null

    fun show(context: Context, text: String) {
        try {
            if (toast != null) {
                toast!!.setText(text)
            } else {
                toast = Toast.makeText(context, text, Toast.LENGTH_SHORT)
            }
            toast?.show()
        } catch (e: Exception) {
            Looper.prepare()
            Toast.makeText(context, text, Toast.LENGTH_LONG).show()
            Looper.loop()
        }
    }

}