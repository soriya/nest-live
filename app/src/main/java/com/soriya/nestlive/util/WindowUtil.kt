package com.soriya.nestlive.util

import android.content.Context

object WindowUtil {

    fun pxToDp(context: Context, px: Int): Float {
        val resources = context.resources
        val displayMetrics = resources.displayMetrics
        return px / displayMetrics.density
    }

    fun dpToPx(context: Context, dp: Int): Float {
        val resources = context.resources
        val displayMetrics = resources.displayMetrics
        return dp * displayMetrics.density
    }

    fun getScreenWidthToDp(context: Context): Float {
        val resources = context.resources
        val displayMetrics = resources.displayMetrics
        return displayMetrics.widthPixels / displayMetrics.density
    }

    fun getScreenHeightToDp(context: Context): Float {
        val resources = context.resources
        val displayMetrics = resources.displayMetrics
        return displayMetrics.heightPixels / displayMetrics.density
    }

}