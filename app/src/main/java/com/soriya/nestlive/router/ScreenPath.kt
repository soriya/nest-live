package com.soriya.nestlive.router

import androidx.annotation.StringRes
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.Person
import androidx.compose.ui.graphics.vector.ImageVector
import com.soriya.nestlive.R

//sealed class ScreenPath(val route: String, @StringRes val title: Int? = null, val icon: ImageVector? = null) {
//    object SignIn: ScreenPath("SignIn")
//    object SignUp: ScreenPath("SignUp")
//    object Home: ScreenPath("Home", R.string.title_home, Icons.Default.Home)
//    object Profile: ScreenPath("Profile", R.string.title_profile, Icons.Default.Person)
//    object Categories: ScreenPath("Categories")
//}

enum class ScreenPath(
    val route: String,
    @StringRes val title: Int,
    val isBottomNav: Boolean = false,
    val isTopBar: Boolean = true,
    val icon: ImageVector? = null
) {
    // route名字要与枚举名一致

    SignIn("SignIn", R.string.title_sign_in),

    SignUp("SignUp", R.string.title_sign_up),

    Home("Home", R.string.title_home, true, icon = Icons.Default.Home),

    Profile("Profile", R.string.title_profile, true, icon = Icons.Default.Person),

    Categories("Categories", R.string.title_categories),

    CategoryDetail("CategoryDetail", R.string.title_category_detail),

    Channels("Channels", R.string.title_channels),

    ChannelDetail("ChannelDetail", R.string.title_channel_detail),

    RecommendLives("RecommendLives", R.string.title_recommend_lives),

    LiveDetail("LiveDetail", R.string.title_live_detail, isTopBar = false),

    MyChannel("MyChannel", R.string.title_my_channel),

    GoLive("GoLive", R.string.title_go_live),

    CameraStreamInfo("CameraStreamInfo", R.string.title_camera_stream_info, isTopBar = false),

    GameStreamInfo("GameStreamInfo", R.string.title_game_stream_info),

    GameStreamLive("GameStreamLive", R.string.title_game_stream_live, isTopBar = false),

    ChannelForm("ChannelForm", R.string.title_channel_form),

    Setting("Setting", R.string.title_setting),

    PublishVideo("PublishVideo", R.string.title_publish_video),

    VideoDetail("VideoDetail", R.string.title_video_detail),

    Search("Search", R.string.title_search)
}

//fun ScreenPath.valueOfRoute(route: String) = ScreenPath.values().find {
//    it.route == route
//}!!