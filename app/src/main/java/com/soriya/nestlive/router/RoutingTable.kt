package com.soriya.nestlive.router

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.navigation.NavController
import androidx.navigation.NavType
import androidx.navigation.compose.composable
import androidx.navigation.createGraph
import androidx.navigation.navArgument
import com.soriya.nestlive.screen.category.CategoriesScreen
import com.soriya.nestlive.screen.category.CategoryDetailScreen
import com.soriya.nestlive.screen.channel.ChannelDetailScreen
import com.soriya.nestlive.screen.channel.ChannelFromScreen
import com.soriya.nestlive.screen.channel.ChannelsScreen
import com.soriya.nestlive.screen.live.CameraStreamInfoScreen
import com.soriya.nestlive.screen.live.GameStreamInfoScreen
import com.soriya.nestlive.screen.live.GameStreamLiveScreen
import com.soriya.nestlive.screen.live.GoLiveScreen
import com.soriya.nestlive.screen.live.LiveDetailScreen
import com.soriya.nestlive.screen.live.PublishVideoScreen
import com.soriya.nestlive.screen.live.RecommendLivesScreen
import com.soriya.nestlive.screen.live.VideoDetailScreen
import com.soriya.nestlive.screen.page.HomeScreen
import com.soriya.nestlive.screen.page.ProfileScreen
import com.soriya.nestlive.screen.profile.SettingScreen
import com.soriya.nestlive.screen.search.SearchScreen
import com.soriya.nestlive.screen.sign.SignInScreen
import com.soriya.nestlive.screen.sign.SignUpScreen

@RequiresApi(Build.VERSION_CODES.Q)
fun routingTable(navController: NavController, startDestination: String) =
    navController.createGraph(startDestination) {
        composable(ScreenPath.SignUp.route) {
            SignUpScreen(
                toSignIn = {
                    navController.navigate(ScreenPath.SignIn.route) {
                        popUpTo(ScreenPath.SignUp.route) {
                            inclusive = true
                        }
                    }
                }
            )
        }
        composable(ScreenPath.SignIn.route) {
            SignInScreen(
                toSignUp = {
                    navController.navigate(ScreenPath.SignUp.route) {
                        popUpTo(ScreenPath.SignIn.route) {
                            inclusive = true
                        }
                    }
                },
                signIn = {
                    navController.popBackStack()
                    navController.navigate(ScreenPath.Home.route)
                }
            )
        }
        composable(ScreenPath.Home.route) {
            HomeScreen(
                toCategories = {
                    navController.navigate(ScreenPath.Categories.route)
                },
                toChannels = {
                    navController.navigate(ScreenPath.Channels.route)
                },
                toRecommendLives = {
                    navController.navigate(ScreenPath.RecommendLives.route)
                },
                toLiveDetail = { id ->
                    navController.navigate("${ScreenPath.LiveDetail.route}/$id")
                },
                toCategoryDetail = { id ->
                    navController.navigate("${ScreenPath.CategoryDetail.route}/$id")
                },
                toChannelDetail = { id ->
                    navController.navigate("${ScreenPath.ChannelDetail.route}/$id")
                },
                toSearch = {
                    navController.navigate(ScreenPath.Search.route)
                }
            )
        }
        composable(ScreenPath.Profile.route) {
            ProfileScreen(
                to = { screenPath ->
                    navController.navigate(screenPath.route)
                }
            )
        }
        composable(ScreenPath.Categories.route) {
            CategoriesScreen(
                toCategoryDetail = { id ->
                    navController.navigate("${ScreenPath.CategoryDetail.route}/$id")
                }
            )
        }
        composable(
            route = "${ScreenPath.CategoryDetail.route}/{categoryId}",
            arguments = listOf(navArgument("categoryId") { type = NavType.LongType })
        ) { backStackEntry ->
            // 获取参数
            val categoryId = backStackEntry.arguments?.getLong("categoryId") ?: 0L
            CategoryDetailScreen(
                categoryId = categoryId,
                toLiveDetail = { id ->
                    navController.navigate("${ScreenPath.LiveDetail.route}/$id")
                },
                toVideoDetail = { id ->
                    navController.navigate("${ScreenPath.VideoDetail.route}/$id")
                }
            )
        }
        composable(route = ScreenPath.Channels.route) {
            ChannelsScreen(
                toChannelDetail = { id ->
                    navController.navigate("${ScreenPath.ChannelDetail.route}/$id")
                }
            )
        }
        composable(
            route = "${ScreenPath.ChannelDetail.route}/{channelId}",
            arguments = listOf(navArgument("channelId") { type = NavType.LongType })
        ) { backStackEntry ->
            // 获取参数
            val channelId = backStackEntry.arguments?.getLong("channelId") ?: 0
            ChannelDetailScreen(
                channelId = channelId,
                toGoLive = {},
                toChannelForm = {},
                toVideoDetail = { id ->
                    navController.navigate("${ScreenPath.VideoDetail.route}/$id")
                },
                toLiveDetail = { id ->
                    navController.navigate("${ScreenPath.LiveDetail.route}/$id")
                }
            )
        }
        composable(route = ScreenPath.RecommendLives.route) {
            RecommendLivesScreen(
                toLiveDetail = { id ->
                    navController.navigate("${ScreenPath.LiveDetail.route}/$id")
                }
            )
        }
        composable(
            route = "${ScreenPath.LiveDetail.route}/{channelId}",
            arguments = listOf(navArgument("channelId") { type = NavType.LongType })
        ) { backStackEntry ->
            val channelId = backStackEntry.arguments?.getLong("channelId") ?: 0
            LiveDetailScreen(
                channelId = channelId,
                toBack = {
                    navController.navigateUp()
                }
            )
        }
        composable(route = ScreenPath.MyChannel.route) {
            ChannelDetailScreen(
                toGoLive = {
                    navController.navigate(ScreenPath.GoLive.route)
                },
                toChannelForm = {
                    navController.navigate(ScreenPath.ChannelForm.route)
                },
                toVideoDetail = { id ->
                    navController.navigate("${ScreenPath.VideoDetail.route}/$id")
                },
                toLiveDetail = { id ->
                    navController.navigate("${ScreenPath.LiveDetail.route}/$id")
                }
            )
        }
        composable(route = ScreenPath.GoLive.route) {
            GoLiveScreen(
                toStreamInfo = {
                    navController.navigate(it.route)
                }
            )
        }
        composable(route = ScreenPath.CameraStreamInfo.route) {
            CameraStreamInfoScreen()
        }
        composable(route = ScreenPath.GameStreamInfo.route) {
            GameStreamInfoScreen(
                toLive = {
                    navController.popBackStack()
                    navController.navigate(ScreenPath.GameStreamLive.route)
                }
            )
        }
        composable(route = ScreenPath.GameStreamLive.route) {
            GameStreamLiveScreen(
                back = {
                    navController.navigateUp()
                }
            )
        }
        composable(route = ScreenPath.ChannelForm.route) {
            ChannelFromScreen(
                back = {
                    navController.navigateUp()
                }
            )
        }
        composable(route = ScreenPath.Setting.route) {
            SettingScreen(
                back = {
                    navController.navigateUp()
                }
            )
        }
        composable(route = ScreenPath.PublishVideo.route) {
            PublishVideoScreen(
                toBack = {
                    navController.navigateUp()
                }
            )
        }
        composable(
            route = "${ScreenPath.VideoDetail.route}/{videoId}",
            arguments = listOf(navArgument("videoId") { type = NavType.LongType })
        ) { backStackEntry ->
            val videoId = backStackEntry.arguments?.getLong("videoId") ?: 0
            VideoDetailScreen(videoId = videoId)
        }
        composable(route = ScreenPath.Search.route) {
            SearchScreen(
                toChannelDetail = { id ->
                    navController.popBackStack()
                    navController.navigate("${ScreenPath.ChannelDetail.route}/$id")
                },
                toVideoDetail = { id ->
                    navController.popBackStack()
                    navController.navigate("${ScreenPath.VideoDetail.route}/$id")
                }
            )
        }
    }