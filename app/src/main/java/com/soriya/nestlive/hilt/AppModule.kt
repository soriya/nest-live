package com.soriya.nestlive.hilt

import com.soriya.nestlive.application.MyApplication
import com.soriya.nestlive.db.AppDataBase
import com.soriya.nestlive.db.dao.ChannelInfoDao
import com.soriya.nestlive.db.dao.UserInfoDao
import com.soriya.nestlive.net.CategoryApi
import com.soriya.nestlive.net.ChannelApi
import com.soriya.nestlive.net.LiveApi
import com.soriya.nestlive.net.RetrofitClient
import com.soriya.nestlive.net.UploadApi
import com.soriya.nestlive.net.UserApi
import com.soriya.nestlive.net.VideoApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class AppModule {

    @Provides
    @Singleton
    fun provideCategoryApi(): CategoryApi = RetrofitClient.categoryApi

    @Provides
    @Singleton
    fun provideChannelApi(): ChannelApi = RetrofitClient.channelApi

    @Provides
    @Singleton
    fun provideLiveApi(): LiveApi = RetrofitClient.liveApi

    @Provides
    @Singleton
    fun provideUserApi(): UserApi = RetrofitClient.userApi

    @Provides
    @Singleton
    fun provideVideoApi(): VideoApi = RetrofitClient.videoApi

    @Provides
    @Singleton
    fun providerUploadApi(): UploadApi = RetrofitClient.uploadApi

    @Provides
    @Singleton
    fun providerUserInfoDao(): UserInfoDao = AppDataBase.getInstance(MyApplication.instance).userInfoDao()

    @Provides
    @Singleton
    fun providerChannelInfoDao(): ChannelInfoDao = AppDataBase.getInstance(MyApplication.instance).channelInfoDao()

}