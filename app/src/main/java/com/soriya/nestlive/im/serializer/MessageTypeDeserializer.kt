package com.soriya.nestlive.im.serializer

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.soriya.nestlive.im.MessageType
import java.lang.reflect.Type

class MessageTypeDeserializer: JsonDeserializer<MessageType> {
    override fun deserialize(
        json: JsonElement?,
        typeOfT: Type?,
        context: JsonDeserializationContext?
    ): MessageType {
        val type = json?.asInt
        return MessageType.values().find {
            it.type == type
        } ?: MessageType.EMPTY
    }
}