package com.soriya.nestlive.im.message

import com.soriya.nestlive.im.MessageType

data class IMMessage (
    val type: MessageType,

    val state: Int,

    val from: Long,

    val to: Long,

    val nickname: String?,

    val data: String?
)