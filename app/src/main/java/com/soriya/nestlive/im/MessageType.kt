package com.soriya.nestlive.im

enum class MessageType(val type: Int) {

    EMPTY(-1000),

    SYSTEM_MESSAGE(1000),

    CHAT_SIGN(1001),

    GROUP_SIGN(1002),

    CHAT_MESSAGE(1003),

    GROUP_MESSAGE(1004),

    CHAT_SIGN_DOWN(1005),

    GROUP_SIGN_DOWN(1006),

    STREAMING_HEART(2001)

}