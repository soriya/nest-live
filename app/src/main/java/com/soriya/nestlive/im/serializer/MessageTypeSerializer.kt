package com.soriya.nestlive.im.serializer

import com.google.gson.JsonElement
import com.google.gson.JsonPrimitive
import com.google.gson.JsonSerializationContext
import com.google.gson.JsonSerializer
import com.soriya.nestlive.im.MessageType
import java.lang.reflect.Type

class MessageTypeSerializer : JsonSerializer<MessageType> {
    override fun serialize(
        src: MessageType?,
        typeOfSrc: Type?,
        context: JsonSerializationContext?
    ): JsonElement {
        return JsonPrimitive(src?.type)
    }
}