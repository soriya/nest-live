package com.soriya.nestlive.application

import android.app.Application
import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.soriya.nestlive.im.NettyClient
import dagger.hilt.android.HiltAndroidApp
import javax.inject.Inject

@HiltAndroidApp
class MyApplication : Application() {

    @Inject
    lateinit var nettyClient: NettyClient // App启动时加载Netty客户端

    companion object {
        lateinit var instance: MyApplication
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }

    val imageResult: MutableLiveData<Uri?> by lazy {
        MutableLiveData()
    }

}