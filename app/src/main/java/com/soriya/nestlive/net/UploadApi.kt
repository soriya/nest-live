package com.soriya.nestlive.net

import com.soriya.nestlive.model.ResponseResult
import okhttp3.MultipartBody
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part

interface UploadApi {

    @Multipart
    @POST("/common/upload/image")
    suspend fun updateImage(@Part file: MultipartBody.Part): ResponseResult.Success<String>

}