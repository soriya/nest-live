package com.soriya.nestlive.net

import com.soriya.nestlive.model.PageResult
import com.soriya.nestlive.model.ResponseResult
import com.soriya.nestlive.model.Video
import okhttp3.MultipartBody
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface VideoApi {

//    @Multipart
    @POST("/api/video/publishVideo")
    suspend fun publishVideo(@Body body: MultipartBody): ResponseResult.Success<Unit?>

    @GET("/api/video/{id}")
    suspend fun getById(@Path("id") id: Long): ResponseResult.Success<Video>

    @GET("/api/video/channelId/{channelId}/{page}/{size}")
    suspend fun getByChannelId(@Path("channelId") channelId: Long, @Path("page") page: Int, @Path("size") size: Int): ResponseResult.Success<PageResult<Video>>

    @GET("/api/video/categoryId/{categoryId}/{page}/{size}")
    suspend fun getByCategoryId(@Path("categoryId") categoryId: Long, @Path("page") page: Int, @Path("size") size: Int): ResponseResult.Success<PageResult<Video>>

    @GET("/api/video/search/{key}/{page}/{size}")
    suspend fun search(@Path("key") key: String, @Path("page") page: Int, @Path("size") size: Int): ResponseResult.Success<PageResult<Video>>

}