package com.soriya.nestlive.net

import com.soriya.nestlive.model.ResponseResult
import com.soriya.nestlive.model.User
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface UserApi {

    @POST("/api/user/login")
    suspend fun login(@Body body: Map<String, String>): ResponseResult.Success<String?>

    @GET("/api/user/info")
    suspend fun info(): ResponseResult.Success<User>

}