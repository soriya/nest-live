package com.soriya.nestlive.net

import com.soriya.nestlive.model.Channel
import com.soriya.nestlive.model.PageResult
import com.soriya.nestlive.model.ResponseResult
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query

interface ChannelApi {

    @GET("/api/channel/myChannel")
    suspend fun getMyChannel(): ResponseResult.Success<Channel?>

    @POST("/api/channel")
    suspend fun createChannel(@Body channel: Channel): ResponseResult.Success<Unit?>

    @GET("/api/channel/follow/{userId}")
    suspend fun getFollowedChannelList(@Path("userId") userId: Long, @Query("page") page: Int?, @Query("size") size: Int?): ResponseResult.Success<PageResult<Channel>>

    @GET("/api/channel/{id}")
    suspend fun getById(@Path("id") id: Long): ResponseResult.Success<Channel>

    @GET("/api/channel/recommend/{page}/{size}")
    suspend fun getRecommendChannelList(@Path("page") page: Int, @Path("size") size: Int): ResponseResult.Success<PageResult<Channel>>

    @POST("/api/channel/follow")
    suspend fun follow(@Body id: Long): ResponseResult.Success<Unit?>

    @GET("/api/channel/search/{key}/{page}/{size}")
    suspend fun search(@Path("key") key: String, @Path("page") page: Int, @Path("size") size: Int): ResponseResult.Success<PageResult<Channel>>

}