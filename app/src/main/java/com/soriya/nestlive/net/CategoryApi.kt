package com.soriya.nestlive.net

import com.soriya.nestlive.model.Category
import com.soriya.nestlive.model.PageResult
import com.soriya.nestlive.model.ResponseResult
import retrofit2.http.GET
import retrofit2.http.Path

interface CategoryApi {

    @GET("/api/category")
    suspend fun categoryAll(): ResponseResult.Success<List<Category>>

    @GET("/api/category/{page}/{size}")
    suspend fun categoryList(@Path("page") page: Int, @Path("size") size: Int): ResponseResult.Success<PageResult<Category>>

    @GET("/api/category/{id}")
    suspend fun categoryDetail(@Path("id") id: Long): ResponseResult.Success<Category>

}