package com.soriya.nestlive.net

import com.soriya.nestlive.model.Live
import com.soriya.nestlive.model.PageResult
import com.soriya.nestlive.model.ResponseResult
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query

interface LiveApi {

    @GET("/api/live/{page}/{size}")
    suspend fun liveList(@Path("page") page: Int, @Path("size") size: Int): ResponseResult.Success<PageResult<Live>>

    @GET("/api/live/channelId/{channelId}")
    suspend fun getByChannelId(@Path("channelId") channelId: Long): ResponseResult.Success<Live>

    @GET("/api/live/categoryId/{categoryId}/{page}/{size}")
    suspend fun getByCategoryId(@Path("categoryId") categoryId: Long, @Path("page") page: Int, @Path("size") size: Int): ResponseResult.Success<PageResult<Live>>

    @POST("/api/live")
    suspend fun createLive(@Body live: Live): ResponseResult.Success<Unit?>

    @GET("/api/live/stopStream")
    suspend fun stopLive(@Query("channelId") channelId: Long): ResponseResult.Success<Unit?>

    @GET("/api/live/history/{channelId}")
    suspend fun getHistory(@Path("channelId") channelId: Long): ResponseResult.Success<List<Live>>

}