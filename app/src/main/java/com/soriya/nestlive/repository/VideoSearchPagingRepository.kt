package com.soriya.nestlive.repository

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.soriya.nestlive.model.Video
import com.soriya.nestlive.net.VideoApi
import javax.inject.Inject

class VideoSearchPagingRepository @Inject constructor(
    private val videoApi: VideoApi
) : PagingSource<Int, Video>() {

    var key = ""

    override fun getRefreshKey(state: PagingState<Int, Video>): Int? {
        return null
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Video> {
        val next = params.key ?: 1

        val pageSize = 5

        return try {
            val videoList = videoApi.search(key, next, pageSize)

            LoadResult.Page(
                data = videoList.data.list,
                prevKey = null,
                nextKey = if (videoList.data.totalPage > videoList.data.currentPage) videoList.data.currentPage + 1 else null
            )
        } catch (e : Exception) {
            LoadResult.Error(e)
        }
    }

}