package com.soriya.nestlive.repository

import com.soriya.nestlive.model.ResponseResult
import com.soriya.nestlive.net.VideoApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import okhttp3.MultipartBody
import okhttp3.RequestBody
import javax.inject.Inject

class VideoRepository @Inject constructor(
    private val videoApi: VideoApi
) {

    fun publishVideo(body: MultipartBody): Flow<ResponseResult> = flow {
        emit(ResponseResult.Loading)
        val liveList = videoApi.publishVideo(body)
        emit(liveList)
    }.catch {
        it.printStackTrace()
        emit(ResponseResult.Failed)
    }.flowOn(Dispatchers.IO)

    fun getById(id: Long): Flow<ResponseResult> = flow {
        emit(ResponseResult.Loading)
        val liveList = videoApi.getById(id)
        emit(liveList)
    }.catch {
        it.printStackTrace()
        emit(ResponseResult.Failed)
    }.flowOn(Dispatchers.IO)

}