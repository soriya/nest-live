package com.soriya.nestlive.repository

import com.soriya.nestlive.model.ResponseResult
import com.soriya.nestlive.net.CategoryApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

class CategoryRepository @Inject constructor(
    private val categoryApi: CategoryApi
) {

    fun all(): Flow<ResponseResult> = flow {
        emit(ResponseResult.Loading)
        val categoryList = categoryApi.categoryAll()
        emit(categoryList)
    }.catch {
        it.printStackTrace()
        emit(ResponseResult.Failed)
    }.flowOn(Dispatchers.IO)

    fun list(page: Int, size: Int): Flow<ResponseResult> = flow {
        emit(ResponseResult.Loading)
        val categoryList = categoryApi.categoryList(page, size)
        emit(categoryList)
    }.catch {
        it.printStackTrace()
        emit(ResponseResult.Failed)
    }.flowOn(Dispatchers.IO)

    fun detail(id: Long): Flow<ResponseResult> = flow {
        emit(ResponseResult.Loading)
        val categoryDetail = categoryApi.categoryDetail(id)
        emit(categoryDetail)
    }.catch {
        it.printStackTrace()
        emit(ResponseResult.Failed)
    }.flowOn(Dispatchers.IO)

}