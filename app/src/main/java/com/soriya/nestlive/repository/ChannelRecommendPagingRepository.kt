package com.soriya.nestlive.repository

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.soriya.nestlive.model.Channel
import com.soriya.nestlive.net.ChannelApi
import javax.inject.Inject

class ChannelRecommendPagingRepository @Inject constructor(
    private val channelApi: ChannelApi
) : PagingSource<Int, Channel>() {
    override fun getRefreshKey(state: PagingState<Int, Channel>): Int? {
        return null
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Channel> {
        val next = params.key ?: 1

        val pageSize = 5

        return try {
            val channelList = channelApi.getRecommendChannelList(next, pageSize)

            LoadResult.Page(
                data = channelList.data.list,
                prevKey = null,
                nextKey = if (channelList.data.totalPage > channelList.data.currentPage) channelList.data.currentPage + 1 else null
            )
        } catch (e : Exception) {
            LoadResult.Error(e)
        }
    }


}