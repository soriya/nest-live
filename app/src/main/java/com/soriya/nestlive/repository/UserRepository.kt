package com.soriya.nestlive.repository

import com.soriya.nestlive.model.ResponseResult
import com.soriya.nestlive.net.UserApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

class UserRepository @Inject constructor(
    private val userApi: UserApi
) {

    fun login(account: String, password: String): Flow<ResponseResult> = flow {
        emit(ResponseResult.Loading)
        val map = mapOf(
            "username" to account,
            "password" to password
        )
        emit(userApi.login(map))
    }.catch {
        it.printStackTrace()
        emit(ResponseResult.Failed)
    }.flowOn(Dispatchers.IO)

    fun info(): Flow<ResponseResult> = flow {
        emit(ResponseResult.Loading)
        emit(userApi.info())
    }.catch {
        it.printStackTrace()
        emit(ResponseResult.Failed)
    }.flowOn(Dispatchers.IO)

}