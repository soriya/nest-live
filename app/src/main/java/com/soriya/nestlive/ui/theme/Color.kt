package com.soriya.nestlive.ui.theme

import androidx.compose.ui.graphics.Color

val MarsGreen = Color(0xFF008C8C) // 马尔斯绿
val KleinBlue = Color(0xFF002FA7) // 克莱因蓝
val HermesOrange = Color(0xFFE85827) // 爱马仕橙
val BurgundyRed = Color(0xFF800020) // 勃艮第红
val TiffanyBlue = Color(0xFF81D8D0) // 蒂芙尼蓝
val MummyBrown = Color(0xFF8F4B28) // 木乃伊棕
val PrussianBlue = Color(0xFF003153) // 普鲁士蓝

val GayPurple = Color(0xFF7858F8) // 基佬紫

val LiveRed = Color(0xFFF65858)