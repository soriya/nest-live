package com.soriya.nestlive.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity("nl_user_info")
data class UserInfoEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "_id", typeAffinity = ColumnInfo.INTEGER)
    val _id: Int,

    @ColumnInfo(name = "user_id", typeAffinity = ColumnInfo.INTEGER)
    val userId: Long,

    @ColumnInfo(name = "account", typeAffinity = ColumnInfo.TEXT)
    val account: String,

    @ColumnInfo(name = "nickname", typeAffinity = ColumnInfo.TEXT)
    val nickname: String,

    @ColumnInfo(name = "avatar", typeAffinity = ColumnInfo.TEXT)
    val avatar: String,

    @ColumnInfo(name = "description", typeAffinity = ColumnInfo.TEXT)
    val description: String,

    @ColumnInfo(name = "gender", typeAffinity = ColumnInfo.INTEGER)
    val gender: Int,

    @ColumnInfo(name = "birth", typeAffinity = ColumnInfo.INTEGER)
    val birth: Long,

    @ColumnInfo(name = "updateTime", typeAffinity = ColumnInfo.INTEGER)
    val updateTime: Long
) {
    companion object {
        val EMPTY = UserInfoEntity(
            0,
            0,
            "",
            "",
            "",
            "",
            3,
            0,
            0
        )
    }
}