package com.soriya.nestlive.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.soriya.nestlive.db.dao.ChannelInfoDao
import com.soriya.nestlive.db.dao.UserInfoDao
import com.soriya.nestlive.db.entity.ChannelInfoEntity
import com.soriya.nestlive.db.entity.UserInfoEntity

@Database(entities = [UserInfoEntity::class, ChannelInfoEntity::class], version = 2)
abstract class AppDataBase : RoomDatabase() {

    companion object {
        private var instance: AppDataBase? = null

        fun getInstance(context: Context): AppDataBase = instance ?: synchronized(this) {
            Room.databaseBuilder(context, AppDataBase::class.java, "nest_live")
                .fallbackToDestructiveMigration()
                .build()
                .also {
                    instance = it
                }
        }
    }

    abstract fun userInfoDao(): UserInfoDao

    abstract fun channelInfoDao(): ChannelInfoDao

}