package com.soriya.nestlive.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.soriya.nestlive.db.entity.ChannelInfoEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface ChannelInfoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(channelInfo: ChannelInfoEntity)

    @Query("SELECT * FROM nl_channel_info LIMIT 1")
    fun selectOne(): Flow<ChannelInfoEntity?>

    @Query("DELETE FROM nl_channel_info")
    fun clear()

}