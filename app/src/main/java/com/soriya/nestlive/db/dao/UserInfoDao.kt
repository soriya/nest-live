package com.soriya.nestlive.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.soriya.nestlive.db.entity.UserInfoEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface UserInfoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(userInfo: UserInfoEntity)

    @Query("SELECT * FROM nl_user_info LIMIT 1")
    fun selectOne(): Flow<UserInfoEntity?>

    @Query("DELETE FROM nl_user_info")
    fun clear()

}