package com.soriya.nestlive.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity("nl_channel_info")
data class ChannelInfoEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "_id", typeAffinity = ColumnInfo.INTEGER)
    val _id: Int,

    @ColumnInfo(name = "channel_id", typeAffinity = ColumnInfo.INTEGER)
    val channelId: Long,

    @ColumnInfo(name = "user_id", typeAffinity = ColumnInfo.INTEGER)
    val userId: Long,

    @ColumnInfo(name = "channel_name", typeAffinity = ColumnInfo.TEXT)
    val channelName: String,

    @ColumnInfo(name = "background_cover", typeAffinity = ColumnInfo.TEXT)
    val backgroundCover: String,

    @ColumnInfo(name = "about", typeAffinity = ColumnInfo.TEXT)
    val about: String = "",

    @ColumnInfo(name = "follows", typeAffinity = ColumnInfo.INTEGER)
    val follows: Int = 0,

    @ColumnInfo(name = "live_state", typeAffinity = ColumnInfo.INTEGER)
    val liveState: Int = 0,

    @ColumnInfo(name = "nickname", typeAffinity = ColumnInfo.TEXT)
    val nickname: String = "",

    @ColumnInfo(name = "avatar", typeAffinity = ColumnInfo.TEXT)
    val avatar: String = "",

    @ColumnInfo(name = "updateTime", typeAffinity = ColumnInfo.INTEGER)
    val updateTime: Long
)