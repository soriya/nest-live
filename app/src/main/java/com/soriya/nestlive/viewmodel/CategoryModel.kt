package com.soriya.nestlive.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import com.soriya.nestlive.model.Category
import com.soriya.nestlive.model.PageResult
import com.soriya.nestlive.model.ResponseResult
import com.soriya.nestlive.repository.CategoryPagingRepository
import com.soriya.nestlive.repository.CategoryRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CategoryModel @Inject constructor(
    private val categoryPagingRepository: CategoryPagingRepository,
    private val categoryRepository: CategoryRepository
) : ViewModel() {

    private val _categoryList = MutableStateFlow(listOf<Category>())

    val categoryList: StateFlow<List<Category>> = _categoryList

    private val _categoryDetail = MutableStateFlow(Category.EMPTY)

    val categoryDetail: StateFlow<Category> = _categoryDetail

    // 分页数据
    val categoryListFlow = Pager(
        PagingConfig(
            pageSize = 5, // 页面大小
            prefetchDistance = 2, // 距离底部还有多少条数据加载下一页
            enablePlaceholders = false, // 控件占位 false不占位
            initialLoadSize = 5 // 初始化时加载多少条数据
        )
    ) {
        categoryPagingRepository
    }.flow

    val categoryAll: Flow<List<Category>>
        get() {
            return flow {
                categoryRepository.all().collect { res ->
                    when (res) {
                        is ResponseResult.Success<*> -> {
                            var categoryList = res.data as List<*>
                            categoryList = categoryList.map { it as Category }
                            emit(categoryList)
                        }
                        is ResponseResult.Failed -> {}
                        is ResponseResult.Loading -> {}
                    }
                }
            }
        }

    init {
        viewModelScope.launch {
            categoryRepository.list(0, 5).collect { res ->
                when (res) {
                    is ResponseResult.Success<*> -> {
                        val pageResult = res.data as PageResult<*>
                        _categoryList.value = pageResult.list.map {
                            it as Category
                        }
                    }
                    is ResponseResult.Failed -> {}
                    is ResponseResult.Loading -> {}
                }
            }
        }
    }

    fun getDetail(id: Long) {
        viewModelScope.launch {
            categoryRepository.detail(id).collect { res ->
                when (res) {
                    is ResponseResult.Success<*> -> {
                        val category = res.data as Category
                        _categoryDetail.emit(category)
                    }
                    is ResponseResult.Failed -> {}
                    is ResponseResult.Loading -> {}
                }
            }
        }
    }

}