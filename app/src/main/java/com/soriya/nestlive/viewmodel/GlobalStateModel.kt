package com.soriya.nestlive.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class GlobalStateModel : ViewModel() {

    private val _hasTopBar = MutableStateFlow(true)

    val hasTopBar: StateFlow<Boolean> = _hasTopBar

    fun changeTopBarState(visible: Boolean) {
        viewModelScope.launch {
            _hasTopBar.emit(visible)
        }
    }

}