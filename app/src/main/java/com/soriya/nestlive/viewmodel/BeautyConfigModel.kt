package com.soriya.nestlive.viewmodel

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

class BeautyConfigModel : ViewModel() {

    private val _grindRatio = MutableStateFlow(0F)

    val grindRatio: StateFlow<Float> = _grindRatio

    private val _whitenRatio = MutableStateFlow(0F)

    val whitenRatio: StateFlow<Float> = _whitenRatio

    private val _ruddyRatio = MutableStateFlow(0F)

    val ruddyRatio: StateFlow<Float> = _ruddyRatio

    suspend fun changeGrindRatio(ratio: Float) {
        _grindRatio.emit(ratio)
    }

    suspend fun changeWhitenRatio(ratio: Float) {
        _whitenRatio.emit(ratio)
    }

    suspend fun changeRuddyRatio(ratio: Float) {
        _ruddyRatio.emit(ratio)
    }

}